﻿using ExploreApilkasiCuti.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web;
using System.Web.Routing;

namespace ExploreApilkasiCuti.Controllers
{
    [NoDirectAccess]
    public class PengajuanDOLController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            string actionName = filterContext.ActionDescriptor.ActionName;
            if (session.IsNewSession || Session["Username"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {
                     { "controller", "LoginPage" },
                     { "action", "Index" }
                });
            }

        }
        // GET: PengajuanDOL

        public ActionResult Index()
        {
            ViewBag.NamaMenu = "Pengajuan DOL";
            ViewBag.Menulist = Session["menu_list"];
            return View();


        }

        [HttpGet]
        public ActionResult Index(PengajuanDOL pjn)
        {
            List<PengajuanDOL> listPjn = new List<PengajuanDOL>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync("api/Pengajuan/Get");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listPjn = JsonConvert.DeserializeObject<List<PengajuanDOL>>(Res);
                    setSideBar();
                    return View(listPjn);
                }

            }
            setSideBar();
            return View(listPjn);
        }

        // GET: PengajuanDOL/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: PengajuanDOL/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PengajuanDOL/Create
        [HttpPost]
        public ActionResult Create(dynamic body)
        {
            string jsonString = "";
            dynamic metadata = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    JObject myObject = JsonConvert.DeserializeObject<JObject>(Convert.ToString(body[0]));
                    var getUser = client.PostAsync("api/pengajuan/createpengajuandandetaildol", new StringContent(JsonConvert.SerializeObject(myObject), Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        var Res = userResult.Content.ReadAsStringAsync().Result;
                        System.Diagnostics.Debug.WriteLine(Res);
                    
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        return Json(new { metaData = metadata, response = Res }, JsonRequestBehavior.AllowGet);
                    }
                    System.Diagnostics.Debug.WriteLine(userResult);
                }
                // TODO: Add insert logic here

                //return RedirectToAction("Index");
                metadata = new
                {
                    code = "400",
                    // message = "Failed to Add New Settings. id: "+ Settings.id_setting +", minimal : "+ Settings.nama_pjn +", retensi :"+ Settings.mandatory
                    message = jsonString
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, ActionName("GetDatatableAll")]
        public ActionResult GetDatatableAll()
        {
            List<PengajuanDOL> listPjn = new List<PengajuanDOL>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                //var getUser = client.GetAsync("api/Pengajuan/Get");
                string GroupID = System.Web.HttpContext.Current.Session["AccountGroupName"].ToString();

                if (GroupID != "ADMIN")
                {
                    var body = new PengajuanDOL
                    {
                        id_karyawan = user
                    };
                    string jsonString = (Newtonsoft.Json.JsonConvert.SerializeObject(body));
                    var getUser = client.PostAsync("api/Pengajuan/GetPengajuanbyUserwithNamaKyw", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                    //var getUser = client.GetAsync($"api/Pengajuan/GetPengajuanByUser/{user}");
                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        var Res = userResult.Content.ReadAsStringAsync().Result;
                        listPjn = JsonConvert.DeserializeObject<List<PengajuanDOL>>(Res);
                    }
                    return Json(new { data = listPjn, JsonRequestBehavior.AllowGet });

                }
                else
                {
                    //var getUser = client.GetAsync($"api/Pengajuan/GetPengajuanDOL");
                    var body = new PengajuanDOL
                    {
                    };
                    string jsonString = (Newtonsoft.Json.JsonConvert.SerializeObject(body));
                    var getUser = client.PostAsync("api/Pengajuan/GetPengajuanDOLwithNamaKyw", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        var Res = userResult.Content.ReadAsStringAsync().Result;
                        listPjn = JsonConvert.DeserializeObject<List<PengajuanDOL>>(Res);
                    }
                    return Json(new { data = listPjn, JsonRequestBehavior.AllowGet });
                }
            }
        }

        [HttpPost, ActionName("GetDataByID")]
        public ActionResult GetDataByID(string id)
        {
            dynamic metadata = "";
            if (id == null)
            {
                metadata = new
                {
                    code = "402",
                    message = "ID Settings tidak boleh kosong."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }

            List<PengajuanDOL> listPjn = new List<PengajuanDOL>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync($"api/Pengajuan/Get/{id}");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listPjn = JsonConvert.DeserializeObject<List<PengajuanDOL>>(Res);
                    if (listPjn != null)
                    {
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        return Json(new { metaData = metadata, response = listPjn }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        metadata = new
                        {
                            code = "400",
                            message = "Data Pengajuan dengan ID tsb tidak ditemukan"
                        };
                        return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    metadata = new
                    {
                        code = "400",
                        message = "Terjadi kesalahan"
                    };
                    return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost, ActionName("GetDataDetailPengajuan")]
        public ActionResult GetDataDetailPengajuan(string id)
        {
            dynamic metadata = "";
            List<PengajuanDOL> pengajuan = new List<PengajuanDOL>();
            List<PengajuanDOLArray> pengajuanArray = new List<PengajuanDOLArray>();
            try
            {
                using (var client = new HttpClient())
                {
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));

                    var body = new PengajuanDOL
                    {
                        id_pengajuan = id
                    };
                    string jsonString = (Newtonsoft.Json.JsonConvert.SerializeObject(body));
                    var getData = client.PostAsync("api/Pengajuan/GetDetailPengajuan", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        var res = dataResult.Content.ReadAsStringAsync().Result;
                        pengajuan = JsonConvert.DeserializeObject <List<PengajuanDOL>>(res);
                        foreach (var item in pengajuan[0].details)
                        {
                            DateTime startDate = DateTime.Parse(item.start_date);
                            DateTime endDate = DateTime.Parse(item.end_date);
                            item.start_date = startDate.ToString("MM-dd-yyyy");
                            item.end_date = endDate.ToString("MM-dd-yyyy");
                        }

                        pengajuanArray.Add(new PengajuanDOLArray()
                        {
                            id_pengajuan = pengajuan[0].id_pengajuan,
                            id_karyawan = pengajuan[0].id_karyawan,
                            id_jenis_pengajuan = pengajuan[0].id_jenis_pengajuan,
                            nama_pengajuan = pengajuan[0].nama_pengajuan,
                            tgl_pengajuan = pengajuan[0].tgl_pengajuan.ToString("MM-dd-yyyy"),
                            durasi = pengajuan[0].durasi,
                            denda_cuti = pengajuan[0].denda_cuti,
                            alamat_cuti = pengajuan[0].alamat_cuti,
                            notelp_cuti = pengajuan[0].notelp_cuti,
                            keterangan_pengajuan = pengajuan[0].keterangan_pengajuan,
                            approve_status = pengajuan[0].approve_status,
                            keterangan_approve = pengajuan[0].keterangan_approve,
                            created_by = pengajuan[0].created_by,
                            created_date = pengajuan[0].created_date,
                            changed_by = pengajuan[0].changed_by,
                            changed_date = pengajuan[0].changed_date,
                            nama_approval = pengajuan[0].nama_approval,
                            nama_karyawan_approval = pengajuan[0].nama_karyawan_approval,
                            id_approval = pengajuan[0].id_approval,
                            approved_by = pengajuan[0].approved_by,
                            link_file = pengajuan[0].link_file,
                            nama_file = pengajuan[0].nama_file,
                            details = pengajuan[0].details
                        });

                        metadata = new
                        {
                            Code = "200",
                            Message = "OK"
                        };
                        return Json(new { response = metadata, data = pengajuanArray }, JsonRequestBehavior.AllowGet);
                    }
                }

                metadata = new
                {
                    Code = "400",
                    Message = "Failed"
                };
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Update(PengajuanDOL pjn)
        {
            dynamic metadata = ""; string jsonString = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    //client.DefaultRequestHeaders.Accept.Clear();
                    //client.DefaultRequestHeaders.Accept.Add(
                    //    new MediaTypeWithQualityHeaderValue("application/json"));

                    var body = new PengajuanDOL
                    {
                        id_pengajuan = pjn.id_pengajuan,
                        tgl_pengajuan = pjn.tgl_pengajuan,
                        id_karyawan = pjn.id_karyawan,
                        id_jenis_pengajuan = pjn.id_jenis_pengajuan,
                        durasi = pjn.durasi,
                        keterangan_pengajuan = pjn.keterangan_pengajuan,
                        approve_status = pjn.approve_status,
                        keterangan_approve = pjn.keterangan_approve,
                        changed_by = user,
                        changed_date = DateTime.Now
                    };
                    jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    var getUser = client.PostAsync("api/Pengajuan/UpdatePengajuan", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        return Json(new { metaData = metadata, response = pjn }, JsonRequestBehavior.AllowGet);
                    }

                }
                // TODO: Add insert logic here

                //return RedirectToAction("Index");
                metadata = new
                {
                    code = "400",
                    message = "Failed to Update Data Settings. " + jsonString
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                metadata = new
                {
                    code = "400",
                    message = "Failed to Update Data Settings."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        //public ActionResult Create(FormCollection collection)

        // GET: PengajuanDOL/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: PengajuanDOL/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: PengajuanDOL/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<PengajuanDOL> listPjn = new List<PengajuanDOL>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync($"api/Pengajuan/Get/{id}");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listPjn = JsonConvert.DeserializeObject<List<PengajuanDOL>>(Res);


                }
            }
            if (listPjn == null)
            {
                return HttpNotFound();
            }
            setSideBar();
            return View(listPjn);
        }
        // POST: PengajuanDOL/Delete/{id_setting}
        [HttpPost, ActionName("DeletePengajuan")]
        public ActionResult DeletePengajuan(string id)
        {
            dynamic metadata = ""; string jsonString = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    var body = new PengajuanDOL
                    {
                        id_pengajuan = id
                    };
                    jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    var getUser = client.PostAsync("api/Pengajuan/DeletePengajuan", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "Success. Berhasil Menghapus Data"
                        };
                        return Json(new { metaData = metadata, response = body }, JsonRequestBehavior.AllowGet);
                    }

                }
                // TODO: Add insert logic here

                //return RedirectToAction("Index");
                metadata = new
                {
                    code = "400",
                    message = "Gagal Menghapus Data" //+ id +" "+ jsonString
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                metadata = new
                {
                    code = "400",
                    message = "Gagal Menghapus Data"
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Detail(string id, string destination)
        {
            ViewBag.Id = id;
            ViewBag.Action = destination;
            setSideBar();
            return View();
        }

        // POST: PengajuanDOL/Create
        [HttpGet]
        public ActionResult GetDateNow()
        {
            string jsonString = "";
            dynamic metadata = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    var getUser = client.GetAsync("api/Pengajuan/GetDateNow");

                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        return Json(new { metaData = metadata, response = userResult.Content.ReadAsStringAsync().Result }, JsonRequestBehavior.AllowGet);
                    }

                }
                // TODO: Add insert logic here

                //return RedirectToAction("Index");
                metadata = new
                {
                    code = "400",
                    // message = "Failed to Add New Settings. id: "+ Settings.id_setting +", minimal : "+ Settings.nama_pjn +", retensi :"+ Settings.mandatory
                    message = jsonString
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetDataPengajuanDetailDOLByID(string id)
        {
            dynamic metadata = "";
            List<PengajuanDetailDOL> listPjn = new List<PengajuanDetailDOL>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync($"api/Pengajuan/GetPengajuanDetailByIdPengajuan/{id}");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listPjn = JsonConvert.DeserializeObject<List<PengajuanDetailDOL>>(Res);
                    for (int i = 0; i < listPjn.Count; i++)
                    {
                        if (listPjn[i].start_date != null) listPjn[i].start_date = DateTime.Parse(listPjn[i].start_date).ToString("yyyy-MM-dd");
                        if (listPjn[i].end_date != null) listPjn[i].end_date = DateTime.Parse(listPjn[i].end_date).ToString("yyyy-MM-dd");
                    }
                    metadata = new
                    {
                        code = "200",
                        message = "OK"
                    };
                    return Json(new { metaData = metadata, response = listPjn }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    metadata = new
                    {
                        code = "400",
                        message = "Terjadi kesalahan"
                    };
                    return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult GetDataLogApprovalByIdPengajuan(string id)
        {
            string jsonString = "";
            dynamic metadata = "";
            List<LogApprovalMdl> list = new List<LogApprovalMdl>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");

                var body = new 
                {
                    id_pengajuan = id
                };
                jsonString = JsonConvert.SerializeObject(body);
                System.Diagnostics.Debug.WriteLine(jsonString);
                var getUser = client.PostAsync("api/LogApproval/GetJoinDataLogApproval", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    list = JsonConvert.DeserializeObject<List<LogApprovalMdl>>(Res);
                    metadata = new
                    {
                        code = "200",
                        message = "OK"
                    };
                    return Json(new { metaData = metadata, response = list }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    metadata = new
                    {
                        code = "400",
                        message = "Terjadi kesalahan"
                    };
                    return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public void setSideBar()
        {
            var link = "/PengajuanDOL";
            IEnumerable<MasterMenuArray> listDataMenuLink = GroupMenuRepository.Instance.getDatabyLink(link);
            ViewBag.Menulist = Session["menu_list"];
            if (listDataMenuLink != null)
            {
                foreach (var item in listDataMenuLink)
                {
                    ViewBag.ParentID = item.id_parent;
                    ViewBag.MenuID = item.id_menu;
                    ViewBag.NamaMenu = item.nama_menu;
                    ViewBag.Menulist = Session["menu_list"];
                }
            }
        }

    }
}
