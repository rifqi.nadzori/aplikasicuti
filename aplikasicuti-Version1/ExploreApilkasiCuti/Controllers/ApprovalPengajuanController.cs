﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ExploreApilkasiCuti.Models;
using Newtonsoft.Json;
using System.Web.Routing;
namespace ExploreApilkasiCuti.Controllers
{
    [NoDirectAccess]
    public class ApprovalPengajuanController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            string actionName = filterContext.ActionDescriptor.ActionName;
            if (session.IsNewSession || Session["Username"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {
                     { "controller", "LoginPage" },
                     { "action", "Index" }
                });
            }

        }
        // GET: ApprovalPengajuan
        public ActionResult Index()
        {

            var link = "/ApprovalPengajuan";
            IEnumerable<MasterMenuArray> listDataMenuLink = GroupMenuRepository.Instance.getDatabyLink(link);
            ViewBag.Menulist = Session["menu_list"];
            if (listDataMenuLink != null)
            {
                foreach (var item in listDataMenuLink)
                {
                    ViewBag.ParentID = item.id_parent;
                    ViewBag.MenuID = item.id_menu;
                    ViewBag.NamaMenu = item.nama_menu;
                    ViewBag.Menulist = Session["menu_list"];

                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Error404");
            }
        }

        [HttpPost, ActionName("LoadDataOutstandingApproval")]
        public ActionResult LoadDataOutstandingApproval(string idKaryawan)
        {
            dynamic metadata = "";
            List<ApprovalPengajuan> listAppPengajuan = new List<ApprovalPengajuan>();
            List<ApprovalPengajuanArray> listAppPengajuanArr = new List<ApprovalPengajuanArray>();
            using (var client = new HttpClient())
            {
                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                var body = new ApprovalPengajuan
                {
                    id_karyawan = idKaryawan
                };
                string jsonString = (Newtonsoft.Json.JsonConvert.SerializeObject(body));
                var getData = client.PostAsync("api/ApprovalPengajuan/GetDataListApproval", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                getData.Wait();

                var dataResult = getData.Result;
                if (dataResult.IsSuccessStatusCode)
                {
                    var res = dataResult.Content.ReadAsStringAsync().Result;
                    listAppPengajuan = JsonConvert.DeserializeObject<List<ApprovalPengajuan>>(res);
                    for (int i = 0; i < listAppPengajuan.Count; i++)
                    {
                        string action = //"<button type='button' class='btn btn-sm btn-icon btn-icon rounded-circle bg-gradient-secondary waves-effect waves-light' style='margin-right: 5px;' onclick=edit_pengajuan('" + listAppPengajuan[i].id_pengajuan + "') disabled><i class='fas fa-edit'></i></button>" + "  " +
                                        "<button type='button' class='btn btn-sm btn-icon btn-icon rounded-circle bg-gradient-success waves-effect waves-light' onclick=detail_pengajuan('" + listAppPengajuan[i].id_pengajuan + "')><i class='fas fa-eye'></i></button>";

                        listAppPengajuanArr.Add(new ApprovalPengajuanArray()
                        {
                            no_urut = i + 1,
                            id_pengajuan = listAppPengajuan[i].id_pengajuan,
                            id_karyawan = listAppPengajuan[i].id_karyawan,
                            nama = listAppPengajuan[i].nama,
                            id_jenis_pengajuan = listAppPengajuan[i].id_jenis_pengajuan,
                            nama_pengajuan = listAppPengajuan[i].nama_pengajuan,
                            tgl_pengajuan = listAppPengajuan[i].tgl_pengajuan?.ToString("MM-dd-yyyy"),
                            durasi = listAppPengajuan[i].durasi,
                            denda_cuti = listAppPengajuan[i].denda_cuti,
                            alamat_cuti = listAppPengajuan[i].alamat_cuti,
                            notelp_cuti = listAppPengajuan[i].notelp_cuti,
                            keterangan_pengajuan = listAppPengajuan[i].keterangan_pengajuan,
                            approve_status = listAppPengajuan[i].approve_status,
                            keterangan_approve = listAppPengajuan[i].keterangan_approve,
                            created_by = listAppPengajuan[i].created_by,
                            created_date = listAppPengajuan[i].created_date?.ToString("MM-dd-yyyy"),
                            changed_by = listAppPengajuan[i].changed_by,
                            changed_date = listAppPengajuan[i].changed_date?.ToString("MM-dd-yyyy"),
                            action = action
                        });
                    }

                    metadata = new
                    {
                        Code = "200",
                        Message = "OK"
                    };
                    return Json(new { response = metadata, data = listAppPengajuanArr }, JsonRequestBehavior.AllowGet);
                }
            }

            metadata = new
            {
                Code = "400",
                Message = "Failed"
            };
            return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("CheckApprovalFinish")]
        public async System.Threading.Tasks.Task<ActionResult> CheckApprovalFinish(string idKaryawan)
        {
            dynamic metadata = "";
            dynamic finishStatus;
            using (var client = new HttpClient())
            {
                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                var body = new ApprovalPengajuan
                {
                    id_karyawan = idKaryawan
                };
                string jsonString = JsonConvert.SerializeObject(body);
                System.Diagnostics.Debug.WriteLine(jsonString);
                HttpResponseMessage getData = await client.PostAsync("api/ApprovalPengajuan/CheckFinishApproval", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                if (getData.IsSuccessStatusCode)
                {
                    String strResult = await getData.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JToken>(strResult);
                    bool resData = (bool)result["finish"];
                    metadata = new
                    {
                        Code = "200",
                        Message = "OK"
                    };
                    return Json(new { response = metadata, data = resData }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    String strResult = await getData.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JToken>(strResult);
                    String res = result["Message"].ToString();
                    metadata = new
                    {
                        Code = "400",
                        Message = res
                    };
                    return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult DetailOutstandingPengajuan(string id, string destination, bool? finish)
        {
            ViewBag.Id = id;
            ViewBag.Action = destination;
            ViewBag.FinishStatus = finish;
            setSideBar();
            return View();
        }

        [HttpPost, ActionName("GetDetailOutstandingApproval")]
        public ActionResult GetDetailOutstandingApproval(string idKaryawan, string idPengajuan)
        {
            dynamic metadata = "";
            List<ApprovalPengajuan> listAppPengajuan = new List<ApprovalPengajuan>();
            List<ApprovalPengajuanArray> listAppPengajuanArr = new List<ApprovalPengajuanArray>();
            using (var client = new HttpClient())
            {
                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                var body = new ApprovalPengajuan
                {
                    id_karyawan = idKaryawan,
                    id_pengajuan = idPengajuan
                };
                string jsonString = (Newtonsoft.Json.JsonConvert.SerializeObject(body));
                var getData = client.PostAsync("api/ApprovalPengajuan/GetDetailOutstandingPengajuan", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                getData.Wait();

                var dataResult = getData.Result;
                if (dataResult.IsSuccessStatusCode)
                {
                    var res = dataResult.Content.ReadAsStringAsync().Result;
                    listAppPengajuan = JsonConvert.DeserializeObject<List<ApprovalPengajuan>>(res);
                    for (int i = 0; i < listAppPengajuan.Count; i++)
                    {
                        foreach(var item in listAppPengajuan[i].details)
                        {
                            DateTime startDate = DateTime.Parse(item.start_date);
                            DateTime endDate = DateTime.Parse(item.end_date);
                            item.start_date = startDate.ToString("MM-dd-yyyy");
                            item.end_date = endDate.ToString("MM-dd-yyyy");
                        }
                        listAppPengajuanArr.Add(new ApprovalPengajuanArray()
                        {
                            id_pengajuan = listAppPengajuan[i].id_pengajuan,
                            id_karyawan = listAppPengajuan[i].id_karyawan,
                            nama = listAppPengajuan[i].nama,
                            id_jenis_pengajuan = listAppPengajuan[i].id_jenis_pengajuan,
                            nama_pengajuan = listAppPengajuan[i].nama_pengajuan,
                            tgl_pengajuan = listAppPengajuan[i].tgl_pengajuan?.ToString("MM-dd-yyyy"),
                            durasi = listAppPengajuan[i].durasi,
                            denda_cuti = listAppPengajuan[i].denda_cuti,
                            alamat_cuti = listAppPengajuan[i].alamat_cuti,
                            notelp_cuti = listAppPengajuan[i].notelp_cuti,
                            keterangan_pengajuan = listAppPengajuan[i].keterangan_pengajuan,
                            approve_status = listAppPengajuan[i].approve_status,
                            keterangan_approve = listAppPengajuan[i].keterangan_approve,
                            created_by = listAppPengajuan[i].created_by,
                            created_date = listAppPengajuan[i].created_date?.ToString("MM-dd-yyyy"),
                            changed_by = listAppPengajuan[i].changed_by,
                            changed_date = listAppPengajuan[i].changed_date?.ToString("MM-dd-yyyy"),
                            link_file = listAppPengajuan[i].link_file,
                            nama_file = listAppPengajuan[i].nama_file,
                            details = listAppPengajuan[i].details
                        });
                    }

                    metadata = new
                    {
                        Code = "200",
                        Message = "OK"
                    };
                    return Json(new { response = metadata, data = listAppPengajuanArr }, JsonRequestBehavior.AllowGet);
                }
            }

            metadata = new
            {
                Code = "400",
                Message = "Failed"
            };
            return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
        }

        [ObjectFilter(Param = "newApprovalPengajuan", RootType = typeof(NewApprovalPengajuan))]
        public async System.Threading.Tasks.Task<ActionResult> SubmitApprovalPengajuan(NewApprovalPengajuan newApprovalPengajuan)
        {
            dynamic metadata = "";
            var s = newApprovalPengajuan;

            try
            {
                using (var client = new HttpClient())
                {
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));

                    var body = new NewApprovalPengajuan
                    {
                        id_karyawan_approval = newApprovalPengajuan.id_karyawan_approval,
                        pengajuan = newApprovalPengajuan.pengajuan,
                        new_id_approval = newApprovalPengajuan.new_id_approval,
                        new_approved_by = newApprovalPengajuan.new_approved_by
                    };

                    string jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    HttpResponseMessage getData = await client.PostAsync("api/ApprovalPengajuan/SubmitApproval", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    if (getData.IsSuccessStatusCode)
                    {
                        String strResult = await getData.Content.ReadAsStringAsync();
                        var result = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JToken>(strResult);
                        String resData = result.ToString();
                        //ViewBag.GeneratedIDPengajuan = resData;
                        metadata = new
                        {
                            Code = "200",
                            Message = "OK"
                        };
                        return Json(new { response = metadata, data = resData }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        String strResult = await getData.Content.ReadAsStringAsync();
                        var result = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JToken>(strResult);
                        String res = result["Message"].ToString();
                        metadata = new
                        {
                            Code = "400",
                            Message = res
                        };
                        return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch
            {
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        public void setSideBar()
        {
            var link = "/ApprovalPengajuan";
            IEnumerable<MasterMenuArray> listDataMenuLink = GroupMenuRepository.Instance.getDatabyLink(link);
            ViewBag.Menulist = Session["menu_list"];
            if (listDataMenuLink != null)
            {
                foreach (var item in listDataMenuLink)
                {
                    ViewBag.ParentID = item.id_parent;
                    ViewBag.MenuID = item.id_menu;
                    ViewBag.NamaMenu = item.nama_menu;
                    ViewBag.Menulist = Session["menu_list"];
                }
            }
        }
    }
}