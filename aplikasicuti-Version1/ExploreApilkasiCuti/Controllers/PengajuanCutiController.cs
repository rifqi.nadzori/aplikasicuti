﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ExploreApilkasiCuti.Models;
using Newtonsoft.Json;
using System.Runtime.Serialization.Json;
using System.Web.Routing;

namespace ExploreApilkasiCuti.Controllers
{
    [NoDirectAccess]
    public class ObjectFilter : ActionFilterAttribute
    {
        public string Param { get; set; }
        public Type RootType { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if ((filterContext.HttpContext.Request.ContentType ?? String.Empty).Contains("application/json"))
            {
                object o = new DataContractJsonSerializer(RootType).ReadObject(filterContext.HttpContext.Request.InputStream);
                filterContext.ActionParameters[Param] = o;
            }
        }
    }
    public class PengajuanCutiController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            string actionName = filterContext.ActionDescriptor.ActionName;
            if (session.IsNewSession || Session["Username"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {
                     { "controller", "LoginPage" },
                     { "action", "Index" }
                });
            }

        }

        // GET: Pengajuan
        public ActionResult Index()
        {

            var link = "/PengajuanCuti";
            IEnumerable<MasterMenuArray> listDataMenuLink = GroupMenuRepository.Instance.getDatabyLink(link);
            ViewBag.Menulist = Session["menu_list"];
            if (listDataMenuLink != null)
            {
                foreach (var item in listDataMenuLink)
                {
                    ViewBag.ParentID = item.id_parent;
                    ViewBag.MenuID = item.id_menu;
                    ViewBag.NamaMenu = item.nama_menu;
                    ViewBag.Menulist = Session["menu_list"];

                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Error404");
            }
        }

        [HttpGet]
        public ActionResult Index(PengajuanCuti pengajuan)
        {
            List<PengajuanCuti> listPengajuan = new List<PengajuanCuti>();
            List<PengajuanCutiArray> listPengajuanArray = new List<PengajuanCutiArray>();
            using (var client = new HttpClient())
            {
                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                var getData = client.GetAsync("api/Pengajuan/Get");
                getData.Wait();

                var dataResult = getData.Result;
                if (dataResult.IsSuccessStatusCode)
                {
                    var res = dataResult.Content.ReadAsStringAsync().Result;
                    listPengajuan = JsonConvert.DeserializeObject<List<PengajuanCuti>>(res);
                    setSideBar();
                    return View(listPengajuan);
                }
            }
            setSideBar();
            return View(listPengajuan);
        }

        [HttpPost, ActionName("LoadDataPengajuanCuti")]
        public ActionResult LoadDataPengajuanCuti()
        {
            dynamic metadata = "";
            List<PengajuanCuti> listPengajuan = new List<PengajuanCuti>();
            List<PengajuanCutiArray> listPengajuanArray = new List<PengajuanCutiArray>();
            using (var client = new HttpClient())
            {
                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                //var getData = client.GetAsync("api/Pengajuan/Get");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                var body = new PengajuanCuti
                {
                };
                string jsonString = (Newtonsoft.Json.JsonConvert.SerializeObject(body));
                var getData = client.PostAsync("api/Pengajuan/GetPengajuanwithNamaKyw", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                getData.Wait();

                var dataResult = getData.Result;
                if (dataResult.IsSuccessStatusCode)
                {
                    var res = dataResult.Content.ReadAsStringAsync().Result;
                    listPengajuan = JsonConvert.DeserializeObject<List<PengajuanCuti>>(res);
                    for (int i = 0; i < listPengajuan.Count; i++)
                    {
                        if (listPengajuan[i].id_pengajuan.StartsWith("CT"))
                        {
                            string action = "<button type='button' class='btn btn-sm btn-icon btn-icon rounded-circle bg-gradient-primary waves-effect waves-light' onclick=detail_pengajuan('" + listPengajuan[i].id_pengajuan + "')><i class='fas fa-eye'></i></button>";
                            // "<button type='button' class='btn btn-sm btn-icon btn-icon rounded-circle bg-gradient-secondary waves-effect waves-light' style='margin-right: 5px;' onclick=edit_pengajuan('" + listPengajuan[i].id_pengajuan + "') disabled><i class='fas fa-edit'></i></button>" + "  " +

                            listPengajuanArray.Add(new PengajuanCutiArray()
                            {
                                no_urut = i + 1,
                                id_pengajuan = listPengajuan[i].id_pengajuan,
                                id_karyawan = listPengajuan[i].id_karyawan,
                                nama = listPengajuan[i].nama,
                                id_jenis_pengajuan = listPengajuan[i].id_jenis_pengajuan,
                                tgl_pengajuan = listPengajuan[i].tgl_pengajuan?.ToString("MM-dd-yyyy"),
                                durasi = listPengajuan[i].durasi,
                                denda_cuti = listPengajuan[i].denda_cuti,
                                alamat_cuti = listPengajuan[i].alamat_cuti,
                                notelp_cuti = listPengajuan[i].notelp_cuti,
                                keterangan_pengajuan = listPengajuan[i].keterangan_pengajuan,
                                approve_status = listPengajuan[i].approve_status,
                                keterangan_approve = listPengajuan[i].keterangan_approve,
                                created_by = listPengajuan[i].created_by,
                                created_date = listPengajuan[i].created_date?.ToString("MM-dd-yyyy"),
                                changed_by = listPengajuan[i].changed_by,
                                changed_date = listPengajuan[i].changed_date?.ToString("MM-dd-yyyy"),
                                action = action
                            });
                        }
                    }
                    string GroupID = System.Web.HttpContext.Current.Session["AccountGroupName"].ToString();

                    if (GroupID != "ADMIN")
                    {

                        listPengajuanArray = listPengajuanArray.Where(e => e.id_karyawan == user).ToList();

                    }
                    
                    metadata = new
                    {
                        Code = "200",
                        Message = "OK"
                    };
                    return Json(new { response = metadata, data = listPengajuanArray }, JsonRequestBehavior.AllowGet);
                }
            }

            metadata = new
            {
                Code = "400",
                Message = "Failed"
            };
            return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult PengajuanCutiPages(string id, string destination)
        {
            ViewBag.Id = id;
            ViewBag.Action = destination;
            setSideBar();
            return View();
        }

        [HttpPost, ActionName("GetDataPengajuanById")]
        public ActionResult GetDataPengajuanById(string id)
        {
            dynamic metadata = "";
            List<PengajuanCuti> listPengajuan = new List<PengajuanCuti>();
            List<PengajuanCutiArray> listPengajuanArray = new List<PengajuanCutiArray>();
            if (id != "")
            {
                using (var client = new HttpClient())
                {
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    var getData = client.GetAsync("api/Pengajuan/Get/" + id);
                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        var res = dataResult.Content.ReadAsStringAsync().Result;
                        listPengajuan = JsonConvert.DeserializeObject<List<PengajuanCuti>>(res);
                        for (int i = 0; i < listPengajuan.Count; i++)
                        {
                            listPengajuanArray.Add(new PengajuanCutiArray()
                            {
                                no_urut = i + 1,
                                id_pengajuan = listPengajuan[i].id_pengajuan,
                                id_karyawan = listPengajuan[i].id_karyawan,
                                id_jenis_pengajuan = listPengajuan[i].id_jenis_pengajuan,
                                tgl_pengajuan = listPengajuan[i].tgl_pengajuan?.ToString("MM-dd-yyyy"),
                                durasi = listPengajuan[i].durasi,
                                denda_cuti = listPengajuan[i].denda_cuti,
                                alamat_cuti = listPengajuan[i].alamat_cuti,
                                notelp_cuti = listPengajuan[i].notelp_cuti,
                                keterangan_pengajuan = listPengajuan[i].keterangan_pengajuan,
                                approve_status = listPengajuan[i].approve_status,
                                keterangan_approve = listPengajuan[i].keterangan_approve,
                                created_by = listPengajuan[i].created_by,
                                created_date = listPengajuan[i].created_date?.ToString("MM-dd-yyyy"),
                                changed_by = listPengajuan[i].changed_by,
                                changed_date = listPengajuan[i].changed_date?.ToString("MM-dd-yyyy"),
                                action = null
                            });
                        }

                        metadata = new
                        {
                            Code = "200",
                            Message = "OK"
                        };

                        return Json(new { response = metadata, data = listPengajuanArray }, JsonRequestBehavior.AllowGet);
                    }
                }

                metadata = new
                {
                    Code = "400",
                    Message = "Failed"
                };

                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
            setSideBar();
            return View("PengajuanCutiPages");
        }

        [HttpPost, ActionName("GetDataDetailPengajuan")]
        public ActionResult GetDataDetailPengajuan(string id)
        {
            dynamic metadata = "";
            List<PengajuanCuti> pengajuan = new List<PengajuanCuti>();
            List<PengajuanCutiArray> pengajuanArray = new List<PengajuanCutiArray>();
            try
            {
                using (var client = new HttpClient())
                {
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));

                    var body = new PengajuanCuti
                    {
                        id_pengajuan = id
                    };
                    string jsonString = (Newtonsoft.Json.JsonConvert.SerializeObject(body));
                    var getData = client.PostAsync("api/Pengajuan/GetDetailPengajuan", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        var res = dataResult.Content.ReadAsStringAsync().Result;
                        pengajuan = JsonConvert.DeserializeObject<List<PengajuanCuti>>(res);
                        foreach (var item in pengajuan[0].details)
                        {
                            DateTime startDate = DateTime.Parse(item.start_date);
                            DateTime endDate = DateTime.Parse(item.end_date);
                            item.start_date = startDate.ToString("MM-dd-yyyy");
                            item.end_date = endDate.ToString("MM-dd-yyyy");
                        }

                        pengajuanArray.Add(new PengajuanCutiArray()
                        {
                            no_urut = 1,
                            id_pengajuan = pengajuan[0].id_pengajuan,
                            id_karyawan = pengajuan[0].id_karyawan,
                            id_jenis_pengajuan = pengajuan[0].id_jenis_pengajuan,
                            nama_pengajuan = pengajuan[0].nama_pengajuan,
                            tgl_pengajuan = pengajuan[0].tgl_pengajuan?.ToString("MM-dd-yyyy"),
                            durasi = pengajuan[0].durasi,
                            denda_cuti = pengajuan[0].denda_cuti,
                            alamat_cuti = pengajuan[0].alamat_cuti,
                            notelp_cuti = pengajuan[0].notelp_cuti,
                            keterangan_pengajuan = pengajuan[0].keterangan_pengajuan,
                            approve_status = pengajuan[0].approve_status,
                            keterangan_approve = pengajuan[0].keterangan_approve,
                            created_by = pengajuan[0].created_by,
                            created_date = pengajuan[0].created_date?.ToString("MM-dd-yyyy"),
                            changed_by = pengajuan[0].changed_by,
                            changed_date = pengajuan[0].changed_date?.ToString("MM-dd-yyyy"),
                            nama_approval = pengajuan[0].nama_approval,
                            nama_karyawan_approval = pengajuan[0].nama_karyawan_approval,
                            id_approval = pengajuan[0].id_approval,
                            approved_by = pengajuan[0].approved_by,
                            nama_file = pengajuan[0].nama_file,
                            link_file = pengajuan[0].link_file,
                            details = pengajuan[0].details
                        });

                        metadata = new
                        {
                            Code = "200",
                            Message = "OK"
                        };
                        return Json(new { response = metadata, data = pengajuanArray }, JsonRequestBehavior.AllowGet);
                    }
                }

                metadata = new
                {
                    Code = "400",
                    Message = "Failed"
                };
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, ActionName("GetDataKaryawanApproval")]
        public ActionResult GetDataKaryawanApproval()
        {
            dynamic metadata = "";
            List<MasterKaryawanApproval> listKywApproval = new List<MasterKaryawanApproval>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                var dataUser = client.GetAsync("api/KaryawanApproval/Get");
                dataUser.Wait();

                var dataResult = dataUser.Result;
                if (dataResult.IsSuccessStatusCode)
                {
                    var Res = dataResult.Content.ReadAsStringAsync().Result;
                    listKywApproval = JsonConvert.DeserializeObject<List<MasterKaryawanApproval>>(Res);

                    metadata = new
                    {
                        Code = "200",
                        Message = "Success"
                    };

                    return Json(new { response = metadata, data = listKywApproval }, JsonRequestBehavior.AllowGet);
                }
            }

            metadata = new
            {
                Code = "400",
                Message = "Failed"
            };

            return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("GetDataDetailPengajuanById")]
        public ActionResult GetDataDetailPengajuanById(string id, string destination)
        {

            dynamic metadata = "";
            List<PengajuanCutiDetail> listDtlPengajuan = new List<PengajuanCutiDetail>();
            List<PengajuanCutiDetailArray> listDtlPengajuanArray = new List<PengajuanCutiDetailArray>();

            if (id != "")
            {
                using (var client = new HttpClient())
                {
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    var getData = client.GetAsync("api/Pengajuan/GetPengajuanDetailByIdPengajuan/" + id);
                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        var res = dataResult.Content.ReadAsStringAsync().Result;
                        listDtlPengajuan = JsonConvert.DeserializeObject<List<PengajuanCutiDetail>>(res);
                        for (int i = 0; i < listDtlPengajuan.Count; i++)
                        {
                            string action1 = "<button type='button' id='btnEditDtl' class='btn btn-sm btn-icon btn-icon rounded-circle bg-gradient-primary waves-effect waves-light' style='margin-right: 5px;' onclick=edit_pengajuandtl('" + listDtlPengajuan[i].id_pengajuan_detail + "')><i class='fas fa-edit'></i></button>" + "  " +
                                            "<button type='button' id='btnDeleteDtl' class='btn btn-sm btn-icon btn-icon rounded-circle bg-gradient-danger waves-effect waves-light' onclick=delete_pengajuandtl('" + listDtlPengajuan[i].id_pengajuan_detail + "')><i class='fa fa-trash-alt'></i></button>";
                            string action2 = "<button type='button' id='btnEditDtl' class='btn btn-sm btn-icon btn-icon rounded-circle bg-gradient-secondary waves-effect waves-light' style='margin-right: 5px;' onclick=edit_pengajuandtl('" + listDtlPengajuan[i].id_pengajuan_detail + "') disabled><i class='fas fa-edit'></i></button>" + "  " +
                                            "<button type='button' id='btnDeleteDtl' class='btn btn-sm btn-icon btn-icon rounded-circle bg-gradient-secondary waves-effect waves-light' onclick=delete_pengajuandtl('" + listDtlPengajuan[i].id_pengajuan_detail + "') disabled><i class='fa fa-trash-alt'></i></button>";

                            listDtlPengajuanArray.Add(new PengajuanCutiDetailArray()
                            {
                                id_pengajuan = listDtlPengajuan[i].id_pengajuan,
                                no = int.Parse(listDtlPengajuan[i].id_pengajuan_detail.Split('_').Last()),
                                id_pengajuan_detail = listDtlPengajuan[i].id_pengajuan_detail,
                                start_date = listDtlPengajuan[i].start_date?.ToString("MM-dd-yyyy"),
                                end_date = listDtlPengajuan[i].end_date?.ToString("MM-dd-yyyy"),
                                durasi = listDtlPengajuan[i].durasi,
                                action = destination == "Detail" ? action2 : action1
                            });;
                        }

                        metadata = new
                        {
                            Code = "200",
                            Message = "OK"
                        };

                        return Json(new { response = metadata, data = listDtlPengajuanArray }, JsonRequestBehavior.AllowGet);
                    }
                }

                metadata = new
                {
                    Code = "400",
                    Message = "Failed"
                };

                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("GetDataPengajuanDtlByIdDtl")]
        public ActionResult GetDataPengajuanDtlByIdDtl(string id)
        {

            dynamic metadata = "";
            List<PengajuanCutiDetail> listDtlPengajuan = new List<PengajuanCutiDetail>();
            List<PengajuanCutiDetailArray> listDtlPengajuanArr = new List<PengajuanCutiDetailArray>();
            if (id != "")
            {
                using (var client = new HttpClient())
                {
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    var getData = client.GetAsync("api/Pengajuan/GetPengajuanDetail/" + id);
                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        var res = dataResult.Content.ReadAsStringAsync().Result;
                        listDtlPengajuan = JsonConvert.DeserializeObject<List<PengajuanCutiDetail>>(res);
                        for (int i = 0; i < listDtlPengajuan.Count; i++)
                        {
                            listDtlPengajuanArr.Add(new PengajuanCutiDetailArray()
                            {
                                id_pengajuan = listDtlPengajuan[i].id_pengajuan,
                                id_pengajuan_detail = listDtlPengajuan[i].id_pengajuan_detail,
                                start_date = listDtlPengajuan[i].start_date?.ToString("MM-dd-yyyy"),
                                end_date = listDtlPengajuan[i].end_date?.ToString("MM-dd-yyyy"),
                                durasi = listDtlPengajuan[i].durasi,
                            });
                        }

                        metadata = new
                        {
                            Code = "200",
                            Message = "OK"
                        };

                        return Json(new { response = metadata, data = listDtlPengajuanArr }, JsonRequestBehavior.AllowGet);
                    }
                }

                metadata = new
                {
                    Code = "400",
                    Message = "Failed"
                };

                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("DeleteDtlById")]
        public ActionResult DeleteDtlById(string id)
        {
            dynamic metadata = "";
            try
            {
                if (id != "")
                {
                    using (var client = new HttpClient())
                    {
                        var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                        var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                        var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                        client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(
                            new MediaTypeWithQualityHeaderValue("application/json"));

                        var body = new PengajuanCutiDetail
                        {
                            id_pengajuan_detail = id
                        };
                        string jsonString = (Newtonsoft.Json.JsonConvert.SerializeObject(body));
                        var getData = client.PostAsync("api/Pengajuan/DeletePengajuanDetail", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                        getData.Wait();

                        var dataResult = getData.Result;
                        if (dataResult.IsSuccessStatusCode)
                        {
                            metadata = new
                            {
                                Code = "200",
                                Message = "OK"
                            };
                            return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                metadata = new
                {
                    Code = "400",
                    Message = "Failed"
                };
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, ActionName("UpdateDetailbyIdDtl")]
        public ActionResult UpdateDetailbyIdDtl(string id)
        {
            dynamic metadata = "";
            try
            {
                DateTime startDate = DateTime.Parse(Request.Form.GetValues("StartDate").FirstOrDefault());
                DateTime endDate = DateTime.Parse(Request.Form.GetValues("EndDate").FirstOrDefault());
                string durasi = Request.Form.GetValues("Waktu").FirstOrDefault(); ;//DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");

                using (var client = new HttpClient())
                {
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));

                    var body = new PengajuanCutiDetail
                    {
                        id_pengajuan_detail = id,
                        start_date = startDate,
                        end_date = endDate,
                        durasi = Convert.ToDecimal(durasi)
                    };
                    string jsonString = (Newtonsoft.Json.JsonConvert.SerializeObject(body));
                    var getData = client.PostAsync("api/Pengajuan/UpdatePengajuanDetail", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        metadata = new
                        {
                            Code = "200",
                            Message = "OK"
                        };
                        return Json(new { response = metadata, data = body }, JsonRequestBehavior.AllowGet);
                    }
                }

                metadata = new
                {
                    Code = "400",
                    Message = "Failed"
                };
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        //[HttpPost, ActionName("CreateNewPengajuanCuti")]
        [ObjectFilter(Param = "newPengajuanCuti", RootType = typeof(NewPengajuanCuti))]
        public async System.Threading.Tasks.Task<ActionResult> CreateNewPengajuanCuti(NewPengajuanCuti newPengajuanCuti)
        {
            dynamic metadata = "";
            var s = newPengajuanCuti;
            NewPengajuanCuti submittedDataReady;
            
            try
            {
                using (var client = new HttpClient())
                {
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));

                    var body = new NewPengajuanCuti
                    {
                        id_pengajuan = "0",
                        id_karyawan = newPengajuanCuti.id_karyawan,
                        id_jenis_pengajuan = newPengajuanCuti.id_jenis_pengajuan,
                        tgl_pengajuan = DateTime.Now,
                        durasi = newPengajuanCuti.durasi,
                        denda_cuti = false,
                        alamat_cuti = newPengajuanCuti.alamat_cuti,
                        notelp_cuti = newPengajuanCuti.notelp_cuti,
                        keterangan_pengajuan = newPengajuanCuti.keterangan_pengajuan,
                        approve_status = "PENDING",
                        keterangan_approve = "",
                        created_by = user,
                        created_date = DateTime.Now,
                        id_approval = newPengajuanCuti.id_approval,
                        approved_by = newPengajuanCuti.approved_by,
                        details = newPengajuanCuti.details
                    };
                    string jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    HttpResponseMessage getData = await client.PostAsync("api/Pengajuan/CreatePengajuan", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    if (getData.IsSuccessStatusCode)
                    {
                        String strResult = await getData.Content.ReadAsStringAsync();
                        var result = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JToken>(strResult);
                        String resData = result["Pengajuan"]["id_pengajuan"].ToString();
                        ViewBag.GeneratedIDPengajuan = resData;
                        metadata = new
                        {
                            Code = "200",
                            Message = "OK"
                        };
                        return Json(new { response = metadata, data = resData }, JsonRequestBehavior.AllowGet);
                    } else
                    {
                        String strResult = await getData.Content.ReadAsStringAsync();
                        var result = JsonConvert.DeserializeObject<Newtonsoft.Json.Linq.JToken>(strResult);
                        String res = result["Message"].ToString();
                        metadata = new
                        {
                            Code = "400",
                            Message = res
                        };
                        return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch
            {
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        public void setSideBar()
        {
            var link = "/PengajuanCuti";
            IEnumerable<MasterMenuArray> listDataMenuLink = GroupMenuRepository.Instance.getDatabyLink(link);
            ViewBag.Menulist = Session["menu_list"];
            if (listDataMenuLink != null)
            {
                foreach (var item in listDataMenuLink)
                {
                    ViewBag.ParentID = item.id_parent;
                    ViewBag.MenuID = item.id_menu;
                    ViewBag.NamaMenu = item.nama_menu;
                    ViewBag.Menulist = Session["menu_list"];
                }
            }
        }
    }
}