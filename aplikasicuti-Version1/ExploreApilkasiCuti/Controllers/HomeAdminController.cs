﻿using ExploreApilkasiCuti.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Mvc;
using System.Web;
using System.Web.Routing;


namespace ExploreApilkasiCuti.Controllers
{
    public class HomeAdminController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            string actionName = filterContext.ActionDescriptor.ActionName;
            if (session.IsNewSession || Session["Username"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {
                     { "controller", "LoginPage" },
                     { "action", "Index" }
                });
            }

        }

        // GET: HomeAdmin
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost, ActionName("LoadDataPengajuanCuti")]
        public ActionResult LoadDataPengajuanCuti()
        {
            dynamic metadata = "";
            List<PengajuanCutiArray> listPengajuan = new List<PengajuanCutiArray>();

            using (var client = new HttpClient())
            {
                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                var getData = client.GetAsync("api/Pengajuan/Get");
                getData.Wait();

                var userResult = getData.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listPengajuan = JsonConvert.DeserializeObject<List<PengajuanCutiArray>>(Res);
                    if (listPengajuan != null)
                    {
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        return Json(new { metaData = metadata, response = listPengajuan }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        metadata = new
                        {
                            code = "400",
                            message = "Data tidak ditemukan"
                        };
                        return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    metadata = new
                    {
                        code = "400",
                        message = "Terjadi kesalahan"
                    };
                    return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost, ActionName("GetSaldoCuti")]
        public ActionResult GetSaldoCuti(string id)
        {
           
            dynamic metadata = ""; string jsonString = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    var body = new MasterKaryawanApproval
                    {
                        id_karyawan = id
                    };

                    jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);

                    var getUser = client.PostAsync("api/Karyawan/GetSaldoCuti", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        
                        var Res = userResult.Content.ReadAsStringAsync().Result;
                        //System.Diagnostics.Debug.WriteLine(Res);

                        metadata = new
                        {
                            code = "200",
                            message = "Success. Berhasil Mengambil Data"
                        };
                        return Json(new { metaData = metadata, response = Res }, JsonRequestBehavior.AllowGet);
                    }

                }
                // TODO: Add insert logic here

                //return RedirectToAction("Index");
                metadata = new
                {
                    code = "400",
                    message = "Gagal Mengambil Data" //+ id +" "+ jsonString
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                metadata = new
                {
                    code = "400",
                    message = "Gagal Mengambil Data"
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }


    }
}