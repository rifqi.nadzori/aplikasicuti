﻿using ExploreApilkasiCuti.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Routing;

namespace ExploreApilkasiCuti.Controllers
{
    [NoDirectAccess]
    public class MasterKaryawanController : Controller
    {
        // GET: MasterKaryawan
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            string actionName = filterContext.ActionDescriptor.ActionName;
            if (session.IsNewSession || Session["Username"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {
                     { "controller", "LoginPage" },
                     { "action", "Index" }
                });
            }

        }

        public ActionResult Index()
        {

            var link = "/MasterKaryawan";
            IEnumerable<MasterMenuArray> listDataMenuLink = GroupMenuRepository.Instance.getDatabyLink(link);
            ViewBag.Menulist = Session["menu_list"];
            if (listDataMenuLink != null)
            {
                foreach (var item in listDataMenuLink)
                {
                    ViewBag.ParentID = item.id_parent;
                    ViewBag.MenuID = item.id_menu;
                    ViewBag.NamaMenu = "Master Karyawan";
                    ViewBag.Menulist = Session["menu_list"];

                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Error404");
            }
        }
        //[HttpGet]
        //public ActionResult Index(MasterKaryawan kr)
        //{

        //    List<MasterKaryawan> listKaryawan = new List<MasterKaryawan>();
        //    List<dynamic> _listval = MasterKaryawanRepository.Instance.getDataMasterKaryawan();
        //    if (_listval[0] == "Success") listKaryawan = _listval[1];
        //    return View(listKaryawan);
        //}

        // GET: MasterKaryawan/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: MasterKaryawan/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MasterKaryawan/Create
        [HttpPost]
        //public ActionResult Create(FormCollection collection)
        public ActionResult Create(MasterKaryawan karyawan)
        {
            dynamic metadata = "";
            List<MasterKaryawan> listKaryawan = new List<MasterKaryawan>();
            try
            {
                using (var client = new HttpClient())
                {
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));


                    //Cek apakah id sudah terdaftar
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);;
                    var getUser = client.GetAsync($"api/Karyawan/Get/{karyawan.id_karyawan}");
                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        var Res = userResult.Content.ReadAsStringAsync().Result;
                        listKaryawan = JsonConvert.DeserializeObject<List<MasterKaryawan>>(Res);
                    }
                    else
                    {
                        metadata = new
                        {
                            code = "400",
                            message = "Terjadi kesalahan"
                        };
                        return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                    }

                    if (listKaryawan.Count > 0)
                    {
                        metadata = new
                        {
                            code = "401",
                            message = "NIK / ID Karyawan sudah terdaftar."
                        };
                        return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                    }

                    //Cek apakah id sudah terdaftar end
                    //Cek Email
                    var validate_email = MasterKaryawanRepository.Instance.ValidateEmail(karyawan.email, karyawan.id_karyawan);
                    if (validate_email != "Success")
                    {
                        metadata = new
                        {
                            code = "402",
                            message = validate_email
                        };
                        return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                    }

                    var body = new MasterKaryawan
                    {
                        id_karyawan = karyawan.id_karyawan,
                        nama = karyawan.nama,
                        alamat = karyawan.alamat,
                        no_telp = karyawan.no_telp,
                        email = karyawan.email,
                        join_date = karyawan.join_date,
                        id_jabatan = karyawan.id_jabatan,
                        password = "123456",
                        status = karyawan.status,
                        created_by = user,
                        created_date = DateTime.Now,
                        id_group = karyawan.id_group
                    };
                    string jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    var createKaryawan = client.PostAsync("api/Karyawan/CreateKaryawan", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    createKaryawan.Wait();

                    var createResult = createKaryawan.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        return Json(new { metaData = metadata, response = karyawan }, JsonRequestBehavior.AllowGet);
                    }

                }

                metadata = new
                {
                    code = "400",
                    message = "Failed to Add New Karyawan."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, ActionName("GetData")]
        public ActionResult GetData()
        {
            List<MasterKaryawan> listKaryawan = new List<MasterKaryawan>();
            List<MasterKaryawanArray> listKrArray = new List<MasterKaryawanArray>();
            var FilterJabatan = Request.Form.GetValues("FilterJabatan").FirstOrDefault();
            var FilterStartDate = Request.Form.GetValues("FilterStartDate").FirstOrDefault();
            var FilterEndDate = Request.Form.GetValues("FilterEndDate").FirstOrDefault();
            var FilterStatus = Request.Form.GetValues("FilterStatus").FirstOrDefault();

            
            List<dynamic> _listval = MasterKaryawanRepository.Instance.getDataMasterKaryawan(FilterJabatan,FilterStartDate,FilterEndDate,FilterStatus);
            if (_listval[0] == "Success") listKrArray = _listval[1];
            return Json(new { data = listKrArray, JsonRequestBehavior.AllowGet });
        }
        [HttpPost, ActionName("GetDataKaryawan")]
        public ActionResult GetDataKaryawan()
        {
            //Tanpa Filter
            List<MasterKaryawan> listKaryawan = new List<MasterKaryawan>();
            List<MasterKaryawanArray> listKrArray = new List<MasterKaryawanArray>();
            

            List<dynamic> _listval = MasterKaryawanRepository.Instance.getDataMasterKaryawan("", "", "", "");
            if (_listval[0] == "Success") listKrArray = _listval[1];
            return Json(listKrArray, JsonRequestBehavior.AllowGet);
        }
        [HttpPost, ActionName("GetDataByID")]
        public ActionResult GetDataByID(string id)
        {
            dynamic metadata = "";
            List<dynamic> _listval;
            List<MasterKaryawanArray> listKaryawan = new List<MasterKaryawanArray>();
            if (id == null)
            {
                metadata = new
                {
                    code = "402",
                    message = "ID Karyawan tidak boleh kosong."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }

            _listval = MasterKaryawanRepository.Instance.getDataMasterKaryawanByID(id);
            if(_listval[0].ToString() == "Success")
            {
                listKaryawan = _listval[1];
                metadata = new
                {
                    code = "200",
                    message = "OK"
                };
                return Json(new { metaData = metadata, response = listKaryawan }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                metadata = new
                {
                    code = "400",
                    message = _listval[1].ToString()
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        //public ActionResult Create(FormCollection collection)
        public ActionResult Update(MasterKaryawan karyawan)
        {
            dynamic metadata = ""; 
            try
            {
                using (var client = new HttpClient())
                {
                    //Cek Email
                    var validate_email = MasterKaryawanRepository.Instance.ValidateEmail(karyawan.email, karyawan.id_karyawan);
                    if (validate_email != "Success")
                    {
                        metadata = new
                        {
                            code = "402",
                            message = validate_email
                        };
                        return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                    }

                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    var body = new MasterKaryawan
                    {
                        id_karyawan = karyawan.id_karyawan,
                        nama = karyawan.nama,
                        alamat = karyawan.alamat,
                        no_telp = karyawan.no_telp,
                        email = karyawan.email,
                        join_date = karyawan.join_date,
                        id_jabatan = karyawan.id_jabatan,
                        //password = "123456",
                        status = karyawan.status,
                        changed_by = user,
                        changed_date = DateTime.Now,
                        id_group = karyawan.id_group
                    };
                    string jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    var getUser = client.PostAsync("api/Karyawan/UpdateKaryawan", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        return Json(new { metaData = metadata, response = karyawan }, JsonRequestBehavior.AllowGet);
                    }

                }
                // TODO: Add insert logic here

                //return RedirectToAction("Index");
                metadata = new
                {
                    code = "400",
                    message = "Failed to Update Data Karyawan."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                metadata = new
                {
                    code = "400",
                    message = "Failed to Update Data Karyawan."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        //public ActionResult Create(FormCollection collection)
        
        // GET: MasterKaryawan/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: MasterKaryawan/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //// GET: MasterKaryawan/Delete/5
        //public ActionResult Delete(string id)
        //{
        //    return View();
        //}

        // GET: MasterKaryawan/GetDataById/5
        [HttpPost, ActionName("GetDataByIdKaryawan")]
        public ActionResult GetDataByIdKaryawan (string id)
        {
            dynamic metadata = "";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<MasterKaryawan> listKaryawan = new List<MasterKaryawan>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync("api/Karyawan/Get/" + id);
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listKaryawan = JsonConvert.DeserializeObject<List<MasterKaryawan>>(Res);

                    metadata = new
                    {
                        code = "200",
                        message = "Success"
                    };

                    return Json(new { response = metadata, data = listKaryawan }, JsonRequestBehavior.AllowGet);
                }
            }
            if (listKaryawan == null)
            {
                return HttpNotFound();
            }
            return View(listKaryawan);
        }
        // POST: MasterKaryawan/Delete/{id_karyawan}
        [HttpPost, ActionName("DeleteKaryawan")]
        public ActionResult DeleteKaryawan(string id)
        {
            dynamic metadata = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    //client.DefaultRequestHeaders.Accept.Clear();
                    //client.DefaultRequestHeaders.Accept.Add(
                    //    new MediaTypeWithQualityHeaderValue("application/json"));

                    var body = new MasterKaryawan
                    {
                        id_karyawan = id
                    };
                    string jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    var getUser = client.PostAsync("api/Karyawan/DeleteKaryawan", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "Success. Berhasil Menghapus Data"
                        };
                        return Json(new { metaData = metadata, response = body }, JsonRequestBehavior.AllowGet);
                    }

                }
                // TODO: Add insert logic here

                //return RedirectToAction("Index");
                metadata = new
                {
                    code = "400",
                    message = "Gagal Menghapus Data"
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                metadata = new
                {
                    code = "400",
                    message = "Gagal Menghapus Data"
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        //Selain Karyawan start
        [HttpPost, ActionName("GetDataJabatan")]
        public ActionResult GetDataJabatan()
        {
            List<MasterJabatan> listJabatan = new List<MasterJabatan>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync("api/Jabatan/Get");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listJabatan = JsonConvert.DeserializeObject<List<MasterJabatan>>(Res);

                }
                return Json(new {listJabatan, JsonRequestBehavior.AllowGet });
            }
        }
        [HttpPost, ActionName("change_password")]
        public ActionResult change_password()
        {
            var valResult = string.Empty;
            string Username = Request.Form.GetValues("Username").FirstOrDefault();
            string current_password = Request.Form.GetValues("current_password").FirstOrDefault();
            string new_password = Request.Form.GetValues("new_password").FirstOrDefault();
            string confirm_new_password = Request.Form.GetValues("confirm_new_password").FirstOrDefault();

            valResult = MasterKaryawanRepository.Instance.ChangePassword(Username, current_password, new_password, confirm_new_password);
            return Json(valResult, JsonRequestBehavior.AllowGet);

        }
    }
}
