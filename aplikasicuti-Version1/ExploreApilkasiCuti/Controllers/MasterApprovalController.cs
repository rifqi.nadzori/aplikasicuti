﻿using ExploreApilkasiCuti.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Mvc;
using System.Web;
using System.Web.Routing;
using System.Linq;

namespace ExploreApilkasiCuti.Controllers
{
    [NoDirectAccess]
    public class MasterApprovalController : Controller
    {
        // GET: MasterApproval
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            string actionName = filterContext.ActionDescriptor.ActionName;
            if (session.IsNewSession || Session["Username"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {
                     { "controller", "LoginPage" },
                     { "action", "Index" }
                });
            }

        }

        public ActionResult Index()
        {

            var link = "/MasterApproval";
            IEnumerable<MasterMenuArray> listDataMenuLink = GroupMenuRepository.Instance.getDatabyLink(link);
            ViewBag.Menulist = Session["menu_list"];
            if (listDataMenuLink != null)
            {
                foreach (var item in listDataMenuLink)
                {
                    ViewBag.ParentID = item.id_parent;
                    ViewBag.MenuID = item.id_menu;
                    ViewBag.NamaMenu = "Master Approval";
                    ViewBag.Menulist = Session["menu_list"];

                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Error404");
            }
        }
        /*
        [HttpGet]
        public ActionResult Index(MasterApproval approval)
        {
            List<MasterApproval> listApproval = new List<MasterApproval>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user =  Session["id_karyawan"];
                var password =  Session["password"];
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "");
                var getUser = client.GetAsync("api/Approval/Get");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listApproval = JsonConvert.DeserializeObject<List<MasterApproval>>(Res);
                    return View(listApproval);
                }

            }
            return View(listApproval);
        }
        */


        // POST: MasterApproval/Create
        [HttpPost]
        public ActionResult Create(MasterApproval approval)
        {
            string jsonString = "";
            dynamic metadata = "";
            List<MasterApproval> listApproval = new List<MasterApproval>();
            try
            {
                using (var client = new HttpClient())
                {
                    //Set Basic Auth
                    var user =  Session["id_karyawan"];
                    var password =  Session["password"];
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));


                    //Cek apakah id sudah terdaftar
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String); ;
                    var getUser = client.GetAsync($"api/Approval/Get/{approval.id_approval}");
                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        var Res = userResult.Content.ReadAsStringAsync().Result;
                        listApproval = JsonConvert.DeserializeObject<List<MasterApproval>>(Res);
                    }
                    else
                    {
                        metadata = new
                        {
                            code = "400",
                            message = "Terjadi kesalahan"
                        };
                        return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                    }

                    if (listApproval.Count > 0)
                    {
                        metadata = new
                        {
                            code = "401",
                            message = "ID Approval sudah terdaftar."
                        };
                        return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                    }


                    //Create Data
                    var body = new MasterApproval
                    {
                        id_approval = approval.id_approval,
                        nama_approval = approval.nama_approval,
                        mandatory = Convert.ToBoolean(approval.mandatory),
                        finish = Convert.ToBoolean(approval.finish),
                        status = approval.status,
                        created_by = user.ToString(),
                        created_date = DateTime.Now
                    };

                    jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    getUser = client.PostAsync("api/Approval/CreateApproval", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        return Json(new { metaData = metadata, response = approval }, JsonRequestBehavior.AllowGet);
                    }

                }
                // TODO: Add insert logic here

                //return RedirectToAction("Index");
                metadata = new
                {
                    code = "400",
                    // message = "Failed to Add New Settings. id: "+ Settings.id_setting +", minimal : "+ Settings.nama_approval +", retensi :"+ Settings.mandatory
                    message = jsonString
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, ActionName("GetDatatableAll")]
        public ActionResult GetDatatableAll()
        {
            List<MasterApproval> listApproval = new List<MasterApproval>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user =  Session["id_karyawan"];
                var password =  Session["password"];
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync("api/Approval/Get");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listApproval = JsonConvert.DeserializeObject<List<MasterApproval>>(Res);

                }

                string GroupID = System.Web.HttpContext.Current.Session["AccountGroupName"].ToString();

                if (GroupID == "Staff")
                {

                    listApproval = listApproval.Where(e => e.id_approval != "APPHRD").ToList();

                }

                return Json(new { data = listApproval, JsonRequestBehavior.AllowGet });
            }
        }

        [HttpPost, ActionName("GetDataByID")]
        public ActionResult GetDataByID(string id)
        {
            dynamic metadata = "";
            if (id == null)
            {
                metadata = new
                {
                    code = "402",
                    message = "ID Settings tidak boleh kosong."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }

            List<MasterApproval> listApproval = new List<MasterApproval>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user =  Session["id_karyawan"];
                var password =  Session["password"];
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync($"api/Approval/Get/{id}");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listApproval = JsonConvert.DeserializeObject<List<MasterApproval>>(Res);
                    if (listApproval != null)
                    {
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        return Json(new { metaData = metadata, response = listApproval }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        metadata = new
                        {
                            code = "400",
                            message = "Data Settings dengan ID tsb tidak ditemukan"
                        };
                        return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    metadata = new
                    {
                        code = "400",
                        message = "Terjadi kesalahan"
                    };
                    return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        [HttpPost]
        public ActionResult Update(MasterApproval approval)
        {
            dynamic metadata = ""; string jsonString = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user =  Session["id_karyawan"];
                    var password =  Session["password"];
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    //client.DefaultRequestHeaders.Accept.Clear();
                    //client.DefaultRequestHeaders.Accept.Add(
                    //    new MediaTypeWithQualityHeaderValue("application/json"));

                    var body = new MasterApproval
                    {
                        id_approval = approval.id_approval,
                        nama_approval = approval.nama_approval,
                        mandatory = Convert.ToBoolean(approval.mandatory),
                        finish = Convert.ToBoolean(approval.finish),
                        status = approval.status,
                        changed_by = user.ToString(),
                        changed_date = DateTime.Now
                    };
                    jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    var getUser = client.PostAsync("api/Approval/UpdateApproval", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        return Json(new { metaData = metadata, response = approval }, JsonRequestBehavior.AllowGet);
                    }

                }
                // TODO: Add insert logic here

                //return RedirectToAction("Index");
                metadata = new
                {
                    code = "400",
                    message = "Failed to Update Data Settings. " + jsonString
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                metadata = new
                {
                    code = "400",
                    message = "Failed to Update Data Settings."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }


        // GET: MasterApproval/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<MasterApproval> listApproval = new List<MasterApproval>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user =  Session["id_karyawan"];
                var password =  Session["password"];
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync($"api/Approval/Get/{id}");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listApproval = JsonConvert.DeserializeObject<List<MasterApproval>>(Res);


                }
            }
            if (listApproval == null)
            {
                return HttpNotFound();
            }
            return View(listApproval);
        }
        // POST: MasterApproval/Delete/{id_setting}
        [HttpPost, ActionName("DeleteApproval")]
        public ActionResult DeleteApproval(string id)
        {
            dynamic metadata = ""; string jsonString = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user =  Session["id_karyawan"];
                    var password =  Session["password"];
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    var body = new MasterApproval
                    {
                        id_approval = id
                    };
                    jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    var getUser = client.PostAsync("api/Approval/DeleteApproval", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "Success. Berhasil Menghapus Data"
                        };
                        return Json(new { metaData = metadata, response = body }, JsonRequestBehavior.AllowGet);
                    }

                }
                // TODO: Add insert logic here

                //return RedirectToAction("Index");
                metadata = new
                {
                    code = "400",
                    message = "Gagal Menghapus Data. ID Approval terdaftar dalam proses Pengajuan." //+ id +" "+ jsonString
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                metadata = new
                {
                    code = "400",
                    message = "Gagal Menghapus Data."// ID Approval terdaftar dalam proses Pengajuan."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, ActionName("GetDataKaryawan")]
        public ActionResult GetDataKaryawan()
        {
            List<MasterKaryawan> listKaryawan = new List<MasterKaryawan>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user =  Session["id_karyawan"];
                var password =  Session["password"];
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync("api/Karyawan/Get");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listKaryawan = JsonConvert.DeserializeObject<List<MasterKaryawan>>(Res);

                }
                return Json(new { data = listKaryawan, JsonRequestBehavior.AllowGet });
                // return Json(listKaryawan, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost, ActionName("DeleteKaryawanApproval_2")]
        public ActionResult DeleteKaryawanApproval_2(string id)
        {
            dynamic metadata = "";
            var statusDelete = "";
            try
            {
                using (var client = new HttpClient())
                {

                    //cek logApproval
                    statusDelete = checkLogApproval(id);
                    if (statusDelete == "false") {
                        metadata = new
                        {
                            code = "400",
                            message = "Gagal Menghapus Data. ID Approval terdaftar dalam proses Pengajuan."
                        };
                        return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                    }


                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user =  Session["id_karyawan"];
                    var password =  Session["password"];
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                   
                    var body = new MasterKaryawanApproval
                    {
                        id_approval = id
                    };
                    string jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    var getUser = client.PostAsync("api/KaryawanApproval/DeleteByIdApproval", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "Success. Berhasil Menghapus Data"
                        };
                        System.Diagnostics.Debug.WriteLine(body);
                        System.Diagnostics.Debug.WriteLine(userResult);

                        return Json(new { metaData = metadata, response = body }, JsonRequestBehavior.AllowGet);

                    }

                }
                // TODO: Add insert logic here

                //return RedirectToAction("Index");
                metadata = new
                {
                    code = "400",
                    message = "Gagal Menghapus Data"
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                metadata = new
                {
                    code = "400",
                    message = "Gagal Menghapus Data"
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        //delete for update/edit data approval
        [HttpPost, ActionName("DeleteKaryawanApproval")]
        public ActionResult DeleteKaryawanApproval(string id)
        {
            dynamic metadata = "";
           
            try
            {
                using (var client = new HttpClient())
                {

                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user = Session["id_karyawan"];
                    var password = Session["password"];
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    var body = new MasterKaryawanApproval
                    {
                        id_approval = id
                    };
                    string jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    var getUser = client.PostAsync("api/KaryawanApproval/DeleteByIdApproval", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "Success. Berhasil Menghapus Data"
                        };
                        System.Diagnostics.Debug.WriteLine(body);
                        System.Diagnostics.Debug.WriteLine(userResult);

                        return Json(new { metaData = metadata, response = body }, JsonRequestBehavior.AllowGet);

                    }

                }
                // TODO: Add insert logic here

                //return RedirectToAction("Index");
                metadata = new
                {
                    code = "400",
                    message = "Gagal Menghapus Data"
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                metadata = new
                {
                    code = "400",
                    message = "Gagal Menghapus Data"
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        //test
        [HttpPost, ActionName("GetDataByIDApproval")]
        public ActionResult GetDataByIDApproval(string id)
        {
            dynamic metadata = "";
            if (id == null)
            {
                metadata = new
                {
                    code = "402",
                    message = "ID Settings tidak boleh kosong."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }

            List<MasterKaryawanApproval> listApproval = new List<MasterKaryawanApproval>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user =  Session["id_karyawan"];
                var password =  Session["password"];
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                //var getUser = client.GetAsync($"api/Approval/Get/{id}");
                var getUser = client.GetAsync($"api/KaryawanApproval/GetByIdApproval/{id}");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listApproval = JsonConvert.DeserializeObject<List<MasterKaryawanApproval>>(Res);
                    if (listApproval != null)
                    {
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        return Json(new { metaData = metadata, response = listApproval }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        metadata = new
                        {
                            code = "400",
                            message = "Data Settings dengan ID tsb tidak ditemukan"
                        };
                        return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    metadata = new
                    {
                        code = "400",
                        message = "Terjadi kesalahan"
                    };
                    return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        
        public string checkLogApproval(string id_Approval)
        {
            List<MasterLogApproval> listLogApproval = new List<MasterLogApproval>();
            var status = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user = Session["id_karyawan"];
                var password = Session["password"];
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync("api/LogApproval/Get");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listLogApproval = JsonConvert.DeserializeObject<List<MasterLogApproval>>(Res);

                }


                List<MasterLogApproval> listLogApproval_2 = new List<MasterLogApproval>();
                listLogApproval_2 = listLogApproval.Where(e => e.id_approval == id_Approval).ToList();
                if (listLogApproval_2.Count > 0) {
                    status = "false";
                }
                else {
                    status = "true";
                }
                System.Diagnostics.Debug.WriteLine("status "+status);
                return status;
            }
        }

    }
}
