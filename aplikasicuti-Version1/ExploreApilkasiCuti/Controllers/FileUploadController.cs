﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Routing;

namespace ExploreApilkasiCuti.Controllers
{
    [NoDirectAccess]
    public class FileUploadController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            string actionName = filterContext.ActionDescriptor.ActionName;
            if (session.IsNewSession || Session["Username"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {
                     { "controller", "LoginPage" },
                     { "action", "Index" }
                });
            }

        }
        // GET: FileUpload
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UploadFiles(HttpPostedFileBase file,string idPengajuan)
        {
            //perlu login untuk test
            /*System.Diagnostics.Debug.WriteLine(file);
            System.Diagnostics.Debug.WriteLine(file.InputStream.Length);
            System.Diagnostics.Debug.WriteLine(file.FileName);
            System.Diagnostics.Debug.WriteLine(idPengajuan);*/
           
            using (var client = new HttpClient())
            {
                using (var content = new MultipartFormDataContent())
                {

                    //Set Basic Auth
                    var user = Session["id_karyawan"];
                    var password = Session["password"];
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));


                    client.BaseAddress = new Uri("http://116.254.101.104/CutiInta/");
                    /*client.BaseAddress = new Uri("https://localhost:44329/");*/
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String); ;
                   


                    byte[] Bytes = new byte[file.InputStream.Length + 1];
                    file.InputStream.Read(Bytes, 0, Bytes.Length);
                    var fileContent = new ByteArrayContent(Bytes);
                    fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") { FileName = file.FileName };
                    content.Add(fileContent);

                    /*var requestUri = "https://localhost:44329/api/Pengajuan/UploadFile/CT202104005";*/
                    

                    var result = client.PostAsync($"api/Pengajuan/UploadFile/{idPengajuan}", content).Result;
                    if (result.StatusCode == System.Net.HttpStatusCode.Created)
                    {
                        List<string> m = result.Content.ReadAsAsync<List<string>>().Result;
                        ViewBag.Success = m.FirstOrDefault();

                    }
                    else
                    {
                        ViewBag.Failed = "Failed !" + result.Content.ToString();
                    }
                }
            }
            return View("Index");
        }
    }
}