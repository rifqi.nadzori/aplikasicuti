﻿using ExploreApilkasiCuti.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ExploreApilkasiCuti.Controllers
{
    public class MasterKaryawanApprovalController : Controller
    {
        // GET: MasterKaryawanApproval
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            string actionName = filterContext.ActionDescriptor.ActionName;
            if (session.IsNewSession || Session["Username"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {
                     { "controller", "LoginPage" },
                     { "action", "Index" }
                });
            }

        }

        public ActionResult Index()
        {
            ViewBag.NamaMenu = "Master Karyawan Approval";
            ViewBag.Menulist = Session["menu_list"];
            return View();


        }
        //[HttpGet]
        //public ActionResult Index(MasterKaryawanApproval kr)
        //{

        //    List<MasterKaryawanApproval> listKaryawan = new List<MasterKaryawanApproval>();
        //    List<dynamic> _listval = MasterKaryawanApprovalRepository.Instance.getDataMasterKaryawanApproval();
        //    if (_listval[0] == "Success") listKaryawan = _listval[1];
        //    return View(listKaryawan);
        //}

        // GET: MasterKaryawanApproval/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: MasterKaryawanApproval/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MasterKaryawanApproval/Create
        [HttpPost]
        //public ActionResult Create(FormCollection collection)
        public ActionResult Create(MasterKaryawanApproval karyawan)
        {
            dynamic metadata = "";
            List<MasterKaryawanApproval> listKaryawan = new List<MasterKaryawanApproval>();
            try
            {
                using (var client = new HttpClient())
                {
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));


                    //Cek apakah id sudah terdaftar
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String); ;
                    var getUser = client.GetAsync($"api/KaryawanApproval/Get/{karyawan.id_karyawan_approval}");
                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        var Res = userResult.Content.ReadAsStringAsync().Result;
                        listKaryawan = JsonConvert.DeserializeObject<List<MasterKaryawanApproval>>(Res);
                    }
                    else
                    {
                        metadata = new
                        {
                            code = "400",
                            message = "Terjadi kesalahan"
                        };
                        return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                    }

                    if (listKaryawan.Count > 0)
                    {
                        metadata = new
                        {
                            code = "401",
                            message = "NIK / ID Karyawan Approval sudah terdaftar."
                        };
                        return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                    }


                    var body = new MasterKaryawanApproval
                    {
                        id_karyawan_approval = karyawan.id_approval + "_" + karyawan.id_karyawan,
                        id_karyawan = karyawan.id_karyawan,
                        id_approval = karyawan.id_approval,
                        created_by = user,
                        created_date = DateTime.Now
                    };
                    string jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    var createKaryawan = client.PostAsync("api/KaryawanApproval/CreateKaryawanApproval", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    createKaryawan.Wait();

                    var createResult = createKaryawan.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                    }

                }

                metadata = new
                {
                    code = "400",
                    message = "Failed to Add New Karyawan Approval."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }




        [HttpPost]
        public ActionResult InsertList(List<Models.MasterKaryawanApproval> karyawan)
        {

            dynamic metadata = "";
            List<MasterKaryawanApproval> listKaryawan = new List<MasterKaryawanApproval>();
            try
            {
                using (var client = new HttpClient())
                {
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));


                    //Cek apakah id sudah terdaftar
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String); ;
                    var getUser = client.GetAsync($"api/KaryawanApproval/Get/{karyawan[0].id_karyawan_approval}");
                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        var Res = userResult.Content.ReadAsStringAsync().Result;
                        listKaryawan = JsonConvert.DeserializeObject<List<MasterKaryawanApproval>>(Res);
                    }
                    else
                    {
                        metadata = new
                        {
                            code = "400",
                            message = "Terjadi kesalahan"
                        };
                        return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                    }

                    if (listKaryawan.Count > 0)
                    {
                        metadata = new
                        {
                            code = "401",
                            message = "NIK / ID Karyawan Approval sudah terdaftar."
                        };
                        return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                    }

                    List<MasterKaryawanApproval> listKaryawan2 = new List<MasterKaryawanApproval>();

                    var body = new MasterKaryawanApproval
                    {
                        id_karyawan_approval = "id_kApp01",
                        id_karyawan = "id_kApp02",
                        id_approval = "APPHRD",
                        created_by = user,
                        created_date = DateTime.Now
                    };
                    var body2 = new MasterKaryawanApproval
                    {
                        id_karyawan_approval = "id_kApp02",
                        id_karyawan = "id_kApp02",
                        id_approval = "APPHRD",
                        created_by = user,
                        created_date = DateTime.Now
                    };

                    listKaryawan2.Add(body);
                    listKaryawan2.Add(body2);
                    var body3 = listKaryawan2;
                    System.Diagnostics.Debug.WriteLine(body3);
                    string jsonString = JsonConvert.SerializeObject(body3);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    var createKaryawan = client.PostAsync("api/KaryawanApproval/CreateKaryawanApprovals", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    createKaryawan.Wait();

                    var createResult = createKaryawan.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        return Json(new { metaData = metadata, response = karyawan }, JsonRequestBehavior.AllowGet);
                        //return Json(new { data = listSetting, JsonRequestBehavior.AllowGet });
                    }

                }

                metadata = new
                {
                    code = "400",
                    message = "Failed to Add New Karyawan Approval."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: MasterKaryawanApproval/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<MasterKaryawanApproval> listKaryawan = new List<MasterKaryawanApproval>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync($"api/KaryawanApproval/Get/{id}");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listKaryawan = JsonConvert.DeserializeObject<List<MasterKaryawanApproval>>(Res);


                }
            }
            if (listKaryawan == null)
            {
                return HttpNotFound();
            }
            return View(listKaryawan);
        }
        [HttpPost, ActionName("DeleteByIdApproval")]
        public ActionResult DeleteByIdApproval(string id)
        {
            dynamic metadata = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                   
                    var body = new MasterKaryawanApproval
                    {
                        id_approval = id
                    };
                    string jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    var getUser = client.PostAsync("api/KaryawanApproval/DeleteByIdApproval", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "Success. Berhasil Menghapus Data"
                        };
                        return Json(new { metaData = metadata, response = body }, JsonRequestBehavior.AllowGet);
                    }

                }
                // TODO: Add insert logic here

                //return RedirectToAction("Index");
                metadata = new
                {
                    code = "400",
                    message = "Gagal Menghapus Data"
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                metadata = new
                {
                    code = "400",
                    message = "Gagal Menghapus Data"
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, ActionName("GetKaryawanApprovalByIdApproval")]
        public ActionResult GetKaryawanApprovalByIdApproval(string id)
        {
            dynamic metadata = "";
            List<MasterKaryawanApprovalJoin> list = new List<MasterKaryawanApprovalJoin>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                var body = new
                {
                    id_approval = id
                };
                string jsonString = JsonConvert.SerializeObject(body);
                var getUser = client.PostAsync("api/KaryawanApproval/GetJoinKaryawanApproval", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    list = JsonConvert.DeserializeObject<List<MasterKaryawanApprovalJoin>>(Res);

                    metadata = new
                    {
                        code = "200",
                        message = "Success"
                    };

                    return Json(new { metaData = metadata, response = list }, JsonRequestBehavior.AllowGet);
                }
            }

            metadata = new
            {
                code = "400",
                message = "Failed"
            };

            return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("GetDataByIdApproval")]
        public ActionResult GetDataByIdApproval(string id)
        {
            dynamic metadata = "";
            List<MasterKaryawanApprovalJoin> listKywApproval = new List<MasterKaryawanApprovalJoin>();
            using (var client = new HttpClient())
            {
                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                //var getData = client.GetAsync("api/KaryawanApproval/GetByIdApproval/" + id);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                var body = new MasterKaryawanApprovalJoin
                {
                    id_approval = id
                };
                string jsonString = (Newtonsoft.Json.JsonConvert.SerializeObject(body));
                var getData = client.PostAsync("api/KaryawanApproval/GetListNamaApprovalbyId", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                getData.Wait();

                var dataResult = getData.Result;
                if (dataResult.IsSuccessStatusCode)
                {
                    var res = dataResult.Content.ReadAsStringAsync().Result;
                    listKywApproval = JsonConvert.DeserializeObject<List<MasterKaryawanApprovalJoin>>(res);

                    metadata = new
                    {
                        Code = "200",
                        Message = "OK"
                    };

                    return Json(new { response = metadata, data = listKywApproval }, JsonRequestBehavior.AllowGet);
                }
            }

            metadata = new
            {
                Code = "400",
                Message = "Failed"
            };

            return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
        }

    }
}