﻿using ExploreApilkasiCuti.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;


namespace ExploreApilkasiCuti.Controllers
{
    [RequireHttps]
    public class HomeController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            string actionName = filterContext.ActionDescriptor.ActionName;
            if (session.IsNewSession || Session["Username"] == null)
             {
                 filterContext.Result = new RedirectToRouteResult(
                 new RouteValueDictionary
                 {
                     { "controller", "LoginPage" },
                     { "action", "Index" }
                 });
             }
            
        }
        public ActionResult Index()
        {
            string GroupID = System.Web.HttpContext.Current.Session["AccountGroupName"].ToString();

            string link = "";
            ViewBag.Menulist = "";
            dynamic metadata = "";
            string html = "";
            html += "<div class='card'>" +
                        "<div class='card-header'>" +
                            "<h2 class='card-title' style='font-weight: bold;'>Outstanding Approval</h2>" +
                        "</div>" +
                        "<div class='card-body'>" +
                        "<input type = 'text' id = 'Periode1'  style = 'width:300px; font-weight:bold;' disabled >"+
                             "<p></p>" +
                                "<div class='row'>" +
                                "<div class='col-lg-4'>" +
                                    "<div class='small-box bg-info'>" +
                                        "<div class='inner'>" +
                                            "<p>Total Outstanding Pengajuan</p>" +
                                            "<h3 id='outstand_Pengajuan'></h3>" +
                                        "</div>" +
                                        "<div class='icon'>" +
                                            "<i class='fas fa-user-plus'></i>" +
                                        "</div>" +
                                    "</div>" +
                                "</div>" +
                                "<div class='col-lg-4'>" +
                                    "<div class='small-box bg-warning'>" +
                                        "<div class='inner'>" +
                                            "<p>Pengajuan Cuti</p>" +
                                            "<h3 id='outstand_Cuti'></h3>" +
                                        "</div>" +
                                        "<div class='icon'>" +
                                            "<i class='fas fa-user-plus'></i>" +
                                        "</div>" +
                                    "</div>" +
                                "</div>" +
                                "<div class='col-lg-4'>" +
                                    "<div class='small-box bg-warning'>" +
                                        "<div class='inner'>" +
                                            "<p>Pengajuan Days On Lieu</p>" +
                                            "<h3 id='outstand_DOL'></h3>" +
                                        "</div>" +
                                        "<div class='icon'>" +
                                            "<i class='fas fa-user-plus'></i>" +
                                        "</div>" +
                                    "</div>" +
                                "</div>" +
                            "</div>" +
                        "</div>" +
                    "</div>";

            if (GroupID == "ADMIN")
            {
                link = "Admin";
            }
            else if (GroupID == "Non_Staff")
            {
                link = "User"; // memiliki aksess Approval
                ViewBag.value = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                ViewBag.value2 = html;
            }
            else if (GroupID == "Staff")
            {
                link = "User"; 
                ViewBag.value = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                
            }
           
            try
            {

                var menu = "";
                menu = getSideBar(GroupID);
                Session["menu_list"]=menu;
                ViewBag.NamaMenu = "Dashboard";
                ViewBag.ParentID = "";
                ViewBag.MenuID = "Home";
                ViewBag.Menulist = menu;
                return PartialView(link);
            }
            catch
            {
            }

            return PartialView(link);
            // return Json(_treeview, JsonRequestBehavior.AllowGet);
        }

        public string getSideBar(string GroupID)
        {
            var menu = "";
            try
            {
                dynamic metadata = "";
                List<GroupMenu> listGroupMenu = new List<GroupMenu>();
                List<GroupMenuArray> GetMenuAccess1 = new List<GroupMenuArray>();
                using (var client = new HttpClient())
                {
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    var getData = client.GetAsync("api/GroupMenu/Get");
                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        var res = dataResult.Content.ReadAsStringAsync().Result;
                        listGroupMenu = JsonConvert.DeserializeObject<List<GroupMenu>>(res);
                        for (int i = 0; i < listGroupMenu.Count; i++)
                        {


                            GetMenuAccess1.Add(new GroupMenuArray()
                            {
                                id_group_menu = listGroupMenu[i].id_group_menu,
                                id_group = listGroupMenu[i].id_group,
                                id_menu = listGroupMenu[i].id_menu,
                                created_by = listGroupMenu[i].created_by,
                                created_date = listGroupMenu[i].created_date
                            });
                        }

                    }
                }

                //Master Menu

                List<MasterMenu> listMtMenu = new List<MasterMenu>();
                List<MasterMenuArray> listDataMenuLink = new List<MasterMenuArray>();
                using (var client = new HttpClient())
                {
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    var getData = client.GetAsync("api/Menu/Get");
                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        var res = dataResult.Content.ReadAsStringAsync().Result;
                        listMtMenu = JsonConvert.DeserializeObject<List<MasterMenu>>(res);
                        for (int i = 0; i < listMtMenu.Count; i++)
                        {
                            if (listMtMenu[i].created_date != null) listMtMenu[i].created_date = listMtMenu[i].created_date.Replace("T00:00:00", "");
                            if (listMtMenu[i].changed_date != null) listMtMenu[i].changed_date = listMtMenu[i].changed_date.Replace("T00:00:00", "");

                            string action = "<button type='button' class='btn btn-sm btn-icon btn-icon rounded-circle bg-gradient-primary waves-effect waves-light' style='margin-right: 5px;' onclick=edit_menu('" + listMtMenu[i].id_menu + "')><i class='fas fa-edit'></i></button>" + "  " +
                                            "<button type='button' class='btn btn-sm btn-icon btn-icon rounded-circle bg-gradient-danger waves-effect waves-light' onclick=delete_menu('" + listMtMenu[i].id_menu + "')><i class='fa fa-trash-alt'></i></button>";

                            listDataMenuLink.Add(new MasterMenuArray()
                            {
                                id_menu = listMtMenu[i].id_menu,
                                id_parent = listMtMenu[i].id_parent,
                                nama_menu = listMtMenu[i].nama_menu,
                                link = listMtMenu[i].link,
                                icon = listMtMenu[i].icon,
                                created_by = listMtMenu[i].created_by,
                                created_date = listMtMenu[i].created_date,
                                changed_by = listMtMenu[i].changed_by,
                                changed_date = listMtMenu[i].changed_date,
                                IsParent = listMtMenu[i].IsParent,
                                MenuOrder = listMtMenu[i].MenuOrder,
                                MenuLevel = listMtMenu[i].MenuLevel,
                                action = action
                            });
                        }

                        metadata = new
                        {
                            Code = "200",
                            Message = "OK"
                        };
                    }
                }

                //SIDEBAR
                //List<MasterMenu> listDataMenuLink = db.MasterMenus.ToList();
                List<GroupMenuArray> GetMenuAccess = new List<GroupMenuArray>();
                GetMenuAccess = GetMenuAccess1.Where(e => e.id_group.ToLower() == GroupID.ToLower()).ToList();

                foreach (var item in listDataMenuLink)
                {
                    var keterangan = string.Empty;
                    foreach (var cekID in GetMenuAccess.Where(m => m.id_menu == item.id_menu))
                    {
                        if (cekID != null)
                        {
                            keterangan = "checked";
                            if (item.IsParent == false && item.id_parent == "0")
                            {
                                menu += "<li class='nav-item'><a href='" + @Url.Content("~/" + item.link + "") + "' class='nav-link' id='" + item.id_menu + "' ><i class='fa " + item.icon + "'></i><p>" + item.nama_menu + "</p></a></li>";

                            }
                            else if (item.IsParent == true && item.MenuLevel == 1)
                            {
                                menu += "<li id='" + item.id_menu + "' class='nav-item has-treeview'>";

                                if (item.IsParent == true && item.id_parent == "0")
                                {
                                    menu += "<a href='#' class='nav-link'><i class='" + item.icon + "'></i><p>" + item.nama_menu + "</p><i class='right fas fa-angle-left'></i></a>";

                                }
                                menu += " <ul class='nav nav-treeview'>";
                                foreach (var child in listDataMenuLink.Where(p => p.id_parent == item.id_menu))
                                {
                                    var MenuID = child.id_menu;
                                    var keterangan1 = string.Empty;

                                    foreach (var cekID1 in GetMenuAccess.Where(m => m.id_menu == child.id_menu))
                                    {
                                        if (cekID1 != null)
                                        {
                                            //keterangan1 = "checked";
                                            if (child.IsParent == true)
                                            {
                                                //menu += "<li>";
                                                //menu += "<label><input type='checkbox' name='id_menu[]' value='" + child.id_menu + "' " + keterangan1 + " /> " + child.nama_menu + "</label>";
                                                //menu += "<ul>";
                                                menu += "<li id='" + child.id_menu + "' class='nav-item has-treeview'>";
                                                menu += "<a href='#' class='nav-link'><i class='" + child.icon + "'></i><p>" + child.nama_menu + "</p><i class='fa fa-angle-left pull-right'></i></a>";
                                                menu += "<ul class='nav nav-treeview'>";

                                                foreach (var child2 in listDataMenuLink.Where(p => p.id_parent == child.id_menu))
                                                {
                                                    var keterangan2 = string.Empty;
                                                    foreach (var cekID2 in GetMenuAccess.Where(m => m.id_menu == child2.id_menu))
                                                    {
                                                        if (cekID2 != null)
                                                        {
                                                            keterangan2 = "checked";
                                                        }
                                                    }
                                                    menu += "<li id='" + child2.id_menu + "' class='nav-item'>";
                                                    menu += "<a href='" + @Url.Content("~/" + child2.link + "") + "' class='nav-link'><i class='" + child2.icon + "'></i><p>" + child2.nama_menu + "</p></a>";
                                                    menu += "</li>";
                                                }
                                                menu += "</ul>";
                                                menu += "</li>";
                                            }
                                            else if (child.IsParent == false && child.id_parent == item.id_menu && child.MenuLevel == 2)
                                            {

                                                menu += "<li class='nav-item'>";
                                                menu += "<a href='" + @Url.Content("~/" + child.link + "") + "' class='nav-link' id='" + child.id_menu + "' ><i class='" + child.icon + "'></i><p>" + child.nama_menu + "</p></a>";
                                                menu += "</li>";
                                            }
                                        }
                                    }

                                }
                                menu += "</ul>";
                                menu += "</li>";
                            }
                        }
                    }

                }

                return menu;
            }
            catch
            {
                return menu; ///System.Diagnostics.Debug.WriteLine("catch");
            }
        }
        

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}