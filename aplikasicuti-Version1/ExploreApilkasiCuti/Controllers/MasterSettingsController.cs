﻿using ExploreApilkasiCuti.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;

namespace ExploreApilkasiCuti.Controllers
{
    [NoDirectAccess]
    public class MasterSettingsController : Controller
    {
        // GET: MasterSettings
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            string actionName = filterContext.ActionDescriptor.ActionName;
            if (session.IsNewSession || Session["Username"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {
                     { "controller", "LoginPage" },
                     { "action", "Index" }
                });
            }

        }
        
        public ActionResult Index()
        {

            var link = "/MasterSettings";
            IEnumerable<MasterMenuArray> listDataMenuLink = GroupMenuRepository.Instance.getDatabyLink(link);
            ViewBag.Menulist = Session["menu_list"];
            if (listDataMenuLink != null)
            {
                foreach (var item in listDataMenuLink)
                {
                    ViewBag.ParentID = item.id_parent;
                    ViewBag.MenuID = item.id_menu;
                    ViewBag.NamaMenu = item.nama_menu;
                    ViewBag.Menulist = Session["menu_list"];

                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Error404");
            }
        }

        [HttpGet]
        public ActionResult Index(MasterSettings setting)
        {
            var html = string.Empty;
            List<MasterSettings> listSetting = new List<MasterSettings>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync("api/Settings/Get");
                getUser.Wait();
                string id = "LamaRetensi";

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    IEnumerable<dynamic> _listval = MasterSettingsRepository.Instance.getMasterSettingbyID(id);
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listSetting = JsonConvert.DeserializeObject<List<MasterSettings>>(Res);
                    if (listSetting.Count() > 0)
                    {
                        foreach (var item in listSetting)
                        {
                            var input = string.Empty;
                            var value = string.Empty;
                            value = @item.value_settings;
                            input += "<input type='number' name='[@i].SettingValue' class='form-control' id='[@i].edit_value_settings' value='" + value + "'  required  />";

                            html += "<div class='col-md-6'>" +
                                        "<div class='box box-default'>" +
                                            "<div class='box-header with-border'>" +
                                                "<h5 class='box-title'><i class='fas fa-cog'></i> " + @item.deskripsi + "</h5>" +
                                            "</div>" +
                                            "<div class='box-body'>" +
                                                "<div class='form-group'>" +
                                                    //"<label for='value' class='col-sm-3 control-label'>Setting Value </label>" +
                                                    "<label for='value' >Setting Value </label>" +
                                                        //"<div class='col-sm-9'>" +
                                                        "<input name='[@i].SettingID' id='[@i].edit_id_setting' value='" + @item.id_setting + "' type='hidden' />" +
                                                        "<input name='[@i].deskripsi' id='[@i].edit_deskripsi' value='" + @item.deskripsi + "' type='hidden' />" +
                                                        input +
                                                    //"</div>" +
                                                "</div>" +
                                            "</div>" +
                                            "<div class='box-footer'>" +
                                            "</div>" +
                                        "</div>" +
                                    "</div>";
                        }
                        setSideBar();
                        ViewBag.value2 = html;    
                    }
                    return View(listSetting);
                }
            }
            setSideBar();
            return View(listSetting);
        }

        // GET: MasterSettings/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: MasterSettings/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MasterSettings/Create
        [HttpPost]
        //public ActionResult Create(FormCollection collection)
        public ActionResult Create(MasterSettings settings)
        {
            string jsonString = "";
            dynamic metadata = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    //client.DefaultRequestHeaders.Accept.Clear();
                    //client.DefaultRequestHeaders.Accept.Add(
                    //    new MediaTypeWithQualityHeaderValue("application/json"));

                    var body = new MasterSettings
                    {
                        id_setting = settings.id_setting,
                        deskripsi = settings.deskripsi,
                        value_settings = settings.value_settings,
                        created_by = user,
                        created_date = DateTime.Now
                    };

                    jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    var getUser = client.PostAsync("api/Settings/CreateSettings", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getUser.Wait(); 

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        return Json(new { metaData = metadata, response = settings }, JsonRequestBehavior.AllowGet);
                    }

                }
                // TODO: Add insert logic here

                //return RedirectToAction("Index");
                metadata = new
                {
                    code = "400",
                    // message = "Failed to Add New Settings. id: "+ Settings.id_setting +", minimal : "+ Settings.minimal_pengajuan +", retensi :"+ Settings.retensi_cuti
                    message = jsonString
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, ActionName("GetDatatableAll")]
        public ActionResult GetDatatableAll()
        {
            List<MasterSettings> listSetting = new List<MasterSettings>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync("api/Settings/Get");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listSetting = JsonConvert.DeserializeObject<List<MasterSettings>>(Res);

                }
                return Json(new { data = listSetting, JsonRequestBehavior.AllowGet });
            }



        }
        [HttpPost, ActionName("GetDataByID")]
        public ActionResult GetDataByID(string id)
        {
            dynamic metadata = "";
            if (id == null)
            {
                metadata = new
                {
                    code = "402",
                    message = "ID Settings tidak boleh kosong."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }

            List<MasterSettings> listSetting = new List<MasterSettings>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync($"api/Settings/Get/{id}");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listSetting = JsonConvert.DeserializeObject<List<MasterSettings>>(Res);
                    if (listSetting != null)
                    {
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        return Json(new { metaData = metadata, response = listSetting }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        metadata = new
                        {
                            code = "400",
                            message = "Data Settings dengan ID tsb tidak ditemukan"
                        };
                        return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    metadata = new
                    {
                        code = "400",
                        message = "Terjadi kesalahan"
                    };
                    return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        [HttpPost]
        public ActionResult Update(MasterSettings settings)
        {
            dynamic metadata = ""; 
            string jsonString = "";
            var body2 = "";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    
                    string[] id_setting2 = Request.Form.GetValues("[@i].SettingID");
                    string[] deskripsi2 = Request.Form.GetValues("[@i].deskripsi");
                    string[] value_settings2 = Request.Form.GetValues("[@i].SettingValue");
                    var size = id_setting2.Count();
                    List<MasterSettings> listSetting = new List<MasterSettings>();
                    for (var no = 0; size > no; no++)
                    {
                        var body = new MasterSettings
                        {
                            id_setting = id_setting2[no],
                            deskripsi = deskripsi2[no],
                            value_settings = value_settings2[no],
                            changed_by = user,
                            changed_date = DateTime.Now
                        };
                        body2 = body.ToString();
                        listSetting.Add(body);
                        
                    }
                jsonString = JsonConvert.SerializeObject(listSetting);
                System.Diagnostics.Debug.WriteLine(jsonString);
                    var getUser = client.PostAsync("api/Settings/UpdateMultipleSettings", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                    getUser.Wait();
                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "OK" + jsonString

                        };
                        //return Json(new { metaData = metadata, response = settings }, JsonRequestBehavior.AllowGet);
                        //return Json(new { metaData = metadata}, JsonRequestBehavior.AllowGet);
                    }
                }
                // TODO: Add insert logic here
                //return RedirectToAction("Index");
                metadata = new
                {
                    code = "400",
                    message = "a Failed to Update Data Settings. "+jsonString+"body nih"+body2
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);    
        }

        /*public ActionResult Update(MasterSettings settings)
        {
            dynamic metadata = "";
            string jsonString = "";

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    //client.DefaultRequestHeaders.Accept.Clear();
                    //client.DefaultRequestHeaders.Accept.Add(
                    //    new MediaTypeWithQualityHeaderValue("application/json"));
                    string[] id_setting2 = Request.Form.GetValues("SettingID[]");
                    string[] deskripsi2 = Request.Form.GetValues("deskripsi[]");
                    string[] value_settings2 = Request.Form.GetValues("SettingValue[]");
                    var size = id_setting2.Count();
                    //for (var no = 0; size > no; no++)

                    //List<SettingValueC1> Data = new List<SettingValueC1>();
                    //Data.Add(new SettingValueC1() { SettingID = SettingID[no], SettingValue = SettingValue[no] });
                    //valResult = SettingValueRepository.Instance.UpdateData(Data);

                    foreach (var item in settings)
                    {
                        var body = new MasterSettings
                        {
                            id_setting = settings.id_setting,
                            deskripsi = settings.deskripsi,
                            value_settings = settings.value_settings,
                            changed_by = user,
                            changed_date = DateTime.Now
                        };
                        jsonString = JsonConvert.SerializeObject(body);
                    }


                    System.Diagnostics.Debug.WriteLine(jsonString);
                    var getUser = client.PostAsync("api/Settings/UpdateSettings", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        return Json(new { metaData = metadata, response = settings }, JsonRequestBehavior.AllowGet);

                    }


                }
                // TODO: Add insert logic here

                //return RedirectToAction("Index");
                metadata = new
                {
                    code = "400",
                    message = "a Failed to Update Data Settings. " + jsonString
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                metadata = new
                {
                    code = "400",
                    message = "Failed to Update Data Settings."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }*/
        //public ActionResult Create(FormCollection collection)

        // GET: MasterSettings/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: MasterSettings/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //// GET: MasterSettings/Delete/5
        //public ActionResult Delete(string id)
        //{
        //    return View();
        //}

        // GET: MasterSettings/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<MasterSettings> listSetting = new List<MasterSettings>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync($"api/Settings/Get/{id}");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listSetting = JsonConvert.DeserializeObject<List<MasterSettings>>(Res);


                }
            }
            if (listSetting == null)
            {
                return HttpNotFound();
            }
            setSideBar();
            return View(listSetting);
        }
        // POST: MasterSettings/Delete/{id_setting}
        [HttpPost, ActionName("DeleteSettings")]
        public ActionResult DeleteSettings(string id)
        {
            dynamic metadata = ""; string jsonString = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    //client.DefaultRequestHeaders.Accept.Clear();
                    //client.DefaultRequestHeaders.Accept.Add(
                    //    new MediaTypeWithQualityHeaderValue("application/json"));

                    var body = new MasterSettings
                    {
                        id_setting = id
                    };
                     jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    var getUser = client.PostAsync("api/Settings/DeleteSettings", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "Success. Berhasil Menghapus Data"
                        };
                        return Json(new { metaData = metadata, response = body }, JsonRequestBehavior.AllowGet);
                    }

                }
                // TODO: Add insert logic here

                //return RedirectToAction("Index");
                metadata = new
                {
                    code = "400",
                    message = "Gagal Menghapus Data" //+ id +" "+ jsonString
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                metadata = new
                {
                    code = "400",
                    message = "Gagal Menghapus Data"
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        public void setSideBar()
        {
            var link = "/MasterSettings";
            IEnumerable<MasterMenuArray> listDataMenuLink = GroupMenuRepository.Instance.getDatabyLink(link);
            ViewBag.Menulist = Session["menu_list"];
            if (listDataMenuLink != null)
            {
                foreach (var item in listDataMenuLink)
                {
                    ViewBag.ParentID = item.id_parent;
                    ViewBag.MenuID = item.id_menu;
                    ViewBag.NamaMenu = item.nama_menu;
                    ViewBag.Menulist = Session["menu_list"];
                }
            }
        }
    }
}
