﻿using ExploreApilkasiCuti.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ExploreApilkasiCuti.Controllers
{
    public class MasterLogApprovalController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            string actionName = filterContext.ActionDescriptor.ActionName;
            if (session.IsNewSession || Session["Username"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {
                     { "controller", "LoginPage" },
                     { "action", "Index" }
                });
            }

        }

        // GET: LogApproval
        /*public ActionResult Index()
        {
            return View();
        }*/
        [HttpPost, ActionName("GetSpecificDataLogApproval")]
        public ActionResult GetSpecificDataLogApproval(string idPengajuan)
        {
            dynamic metadata = "";
            List<MasterLogApproval> listLogApproval = new List<MasterLogApproval>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));

                    var body = new MasterLogApproval
                    {
                        id_pengajuan = idPengajuan
                    };
                    string jsonString = (Newtonsoft.Json.JsonConvert.SerializeObject(body));
                    var getData = client.PostAsync("api/LogApproval/GetSpecificDataLogApproval", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        var Res = dataResult.Content.ReadAsStringAsync().Result;
                        listLogApproval = JsonConvert.DeserializeObject<List<MasterLogApproval>>(Res);

                        metadata = new
                        {
                            Code = "200",
                            Message = "Success"
                        };

                        return Json(new { response = metadata, data = listLogApproval }, JsonRequestBehavior.AllowGet);
                    }
                }

                metadata = new
                {
                    Code = "400",
                    Message = "Failed"
                };

                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            } catch
            {
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, ActionName("GetSpecificDataLogAppReturnElm")]
        public ActionResult GetSpecificDataLogAppReturnElm(string idPengajuan)
        {
            dynamic metadata = "";
            List<MasterLogApprovalJoin> listLogApproval = new List<MasterLogApprovalJoin>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));

                    var body = new MasterLogApproval
                    {
                        id_pengajuan = idPengajuan
                    };
                    string jsonString = (Newtonsoft.Json.JsonConvert.SerializeObject(body));
                    var getData = client.PostAsync("api/LogApproval/GetSpecificDataLogApproval", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        var Res = dataResult.Content.ReadAsStringAsync().Result;
                        listLogApproval = JsonConvert.DeserializeObject<List<MasterLogApprovalJoin>>(Res);
                        string element = "";

                        foreach (var item in listLogApproval)
                        {
                            if(item.approve_status.ToLower() == "approved")
                            {
                                element += "<div id='approve'><i class='fa fa-check-circle bg-success'></i><div class='timeline-item'><span class='time' id='dateApproval'><i class='far fa-calendar-check'></i> " + ((DateTime)item.created_date).ToString("MM/dd/yyyy") + "</span><h3 class='timeline-header' id='statusApproval' style='font-weight:bold'>Status Approval: " + item.approve_status + "</h3><div class='timeline-body'><strong id='namaApproval' class='text-primary'>" + item.nama_approval + " - " + item.nama_karyawan_approval + "</strong> menerima pengajuan cuti Anda</div></div></div>";
                            } 
                            else if (item.approve_status.ToLower() == "forwarded")
                            {
                                element += "<div id='approve'><i class='fa fa-arrow-alt-circle-right bg-primary'></i><div class='timeline-item'><span class='time' id='dateApproval'><i class='far fa-calendar-check'></i> " + ((DateTime)item.created_date).ToString("MM/dd/yyyy") + "</span><h3 class='timeline-header' id='statusApproval' style='font-weight:bold'>Status Approval: " + item.approve_status + "</h3><div class='timeline-body'><strong id='namaApproval' class='text-primary'>" + item.nama_approval + " - " + item.nama_karyawan_approval + "</strong> meneruskan pengajuan cuti Anda</div></div></div>";
                            }
                            else if (item.approve_status.ToLower() == "rejected")
                            {
                                element += "<div id='approve'><i class='fa fa-times-circle bg-danger'></i><div class='timeline-item'><span class='time' id='dateApproval'><i class='far fa-calendar-check'></i> " + ((DateTime)item.created_date).ToString("MM/dd/yyyy") + "</span><h3 class='timeline-header' id='statusApproval' style='font-weight:bold'>Status Approval: " + item.approve_status + "</h3><div class='timeline-body'><strong id='namaApproval' class='text-primary'>" + item.nama_approval + " - " + item.nama_karyawan_approval + "</strong> menolak pengajuan cuti Anda</div></div></div>";
                            }
                            else if (item.approve_status.ToLower() == "pending")
                            {
                                element += "<div id='approve'><i class='fa fa-clock bg-warning'></i><div class='timeline-item'><span class='time' id='dateApproval'><i class='far fa-calendar-check'></i> " + ((DateTime)item.created_date).ToString("MM/dd/yyyy") + "</span><h3 class='timeline-header' id='statusApproval' style='font-weight:bold'>Status Approval: " + item.approve_status + "</h3><div class='timeline-body'>Menunggu approval dari <strong id='namaApproval' class='text-primary'>" + item.nama_approval + " - " + item.nama_karyawan_approval + "</strong></div></div></div>";
                            }
                        }

                        element += "<div><i class='fa fa-circle'></i></div>";

                        metadata = new
                        {
                            Code = "200",
                            Message = "Success"
                        };

                        return Json(new { response = metadata, data = element }, JsonRequestBehavior.AllowGet);
                    }
                }

                metadata = new
                {
                    Code = "400",
                    Message = "Failed"
                };

                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}