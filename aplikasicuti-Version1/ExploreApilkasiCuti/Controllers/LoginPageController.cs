﻿using ExploreApilkasiCuti.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web;

namespace ExploreApilkasiCuti.Controllers
{
    public class LoginPageController : Controller
    {
        // GET: LoginPage
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Index1()
        {
            return View();
        }

        // POST: MasterKaryawan/Delete/{id_karyawan}
        [HttpPost]
        public ActionResult verifySignIn(dynamic body)
        {
            dynamic metadata = "";
            MasterKaryawan dataKaryawan = new MasterKaryawan();

            dataKaryawan = JsonConvert.DeserializeObject<MasterKaryawan>(body[0]);
            var emailLogin = dataKaryawan.email;
            var passwordLogin = dataKaryawan.password;

            //System.Diagnostics.Debug.WriteLine("login  " + email);


            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user = emailLogin;
                    var xpassword = passwordLogin;

                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{xpassword}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    JObject myObject = JsonConvert.DeserializeObject<JObject>(Convert.ToString(body[0]));
                    var getUser = client.PostAsync("api/Karyawan/signInUser", new StringContent(JsonConvert.SerializeObject(myObject), Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        var res = userResult.Content.ReadAsStringAsync().Result;
                        dataKaryawan = JsonConvert.DeserializeObject<MasterKaryawan>(res);
                        Session["Username"] = dataKaryawan.nama;
                        Session["id_karyawan"] = dataKaryawan.id_karyawan;
                        Session["password"] = (string)myObject.SelectToken("password");
                        Session["AccountGroupName"] = dataKaryawan.id_group;
                        //cek status

                        var status = "";
                        if (dataKaryawan.status == "Aktif")
                        {
                            metadata = new
                            {
                                code = "200",
                                message = "Success Login."
                            };
                            return Json(new { metaData = metadata, response = dataKaryawan }, JsonRequestBehavior.AllowGet);
                        }
                        else {
                           
                            metadata = new
                            {
                                code = "400",
                                message = "Gagal Login. Akun Anda Tidak Aktif."
                            };
                            return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);

                        }


                    }

                }

                metadata = new
                {
                    code = "400",
                    message = "Gagal Login"
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                metadata = new
                {
                    code = "400",
                    message = "Gagal Login"
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult LogOut()
        {
           // string Username = System.Web.HttpContext.Current.Session["Username"].ToString();
          //  string id_karyawan = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();

            IDictionaryEnumerator enumerator = System.Web.HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                System.Web.HttpContext.Current.Cache.Remove((string)enumerator.Key);
            }

            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            return RedirectToAction("Index", "LoginPage");
        }
        public ActionResult UserGuide()
        {
            return File(@Url.Content("~/Images/UserGuide_CI.pdf"), "application/pdf");
        }
    }
}