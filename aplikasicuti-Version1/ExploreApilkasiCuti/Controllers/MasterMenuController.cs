﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ExploreApilkasiCuti.Models;
using Newtonsoft.Json;
using System.Web.Routing;

namespace ExploreApilkasiCuti.Controllers
{
    [NoDirectAccess]
    public class MasterMenuController : Controller
    {
        // GET: MasterJabatan
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            string actionName = filterContext.ActionDescriptor.ActionName;
            if (session.IsNewSession || Session["Username"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {
                     { "controller", "LoginPage" },
                     { "action", "Index" }
                });
            }

        }

        public ActionResult Index()
        {

            var link = "/MasterMenu";
            IEnumerable<MasterMenuArray> listDataMenuLink = GroupMenuRepository.Instance.getDatabyLink(link);
            ViewBag.Menulist = Session["menu_list"];
            if (listDataMenuLink != null)
            {
                foreach (var item in listDataMenuLink)
                {
                    ViewBag.ParentID = item.id_parent;
                    ViewBag.MenuID = item.id_menu;
                    ViewBag.NamaMenu = "MasterMenu";
                    ViewBag.Menulist = Session["menu_list"];

                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Error404");
            }
        }
        /*
        [HttpGet]
        public ActionResult Index(MasterMenu masterMenu)
        {
            List<MasterMenu> listMtMenu = new List<MasterMenu>();
            using (var client = new HttpClient())
            {
                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                var getData = client.GetAsync("api/Menu/Get");
                getData.Wait();

                var dataResult = getData.Result;
                if(dataResult.IsSuccessStatusCode)
                {
                    var res = dataResult.Content.ReadAsStringAsync().Result;
                    listMtMenu = JsonConvert.DeserializeObject < List < MasterMenu >> (res);
                    for(int i=0; i< listMtMenu.Count; i++)
                    {
                        if(listMtMenu[i].created_date != null) listMtMenu[i].created_date = listMtMenu[i].created_date.Replace("T00:00:00", "");
                        if(listMtMenu[i].changed_date != null) listMtMenu[i].changed_date = listMtMenu[i].changed_date.Replace("T00:00:00", "");
                    }
                    return View(listMtMenu);
                }
            }
            return View(listMtMenu);
        }
        */
        [HttpPost, ActionName("LoadDataMasterMenu")]
        public ActionResult LoadDataMasterMenu()
        {
            dynamic metadata = "";
            List<MasterMenu> listMtMenu = new List<MasterMenu>();
            List<MasterMenuArray> listMtMenuArr = new List<MasterMenuArray>();
            using (var client = new HttpClient())
            {
                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                var getData = client.GetAsync("api/Menu/Get");
                getData.Wait();

                var dataResult = getData.Result;
                if (dataResult.IsSuccessStatusCode)
                {
                    var res = dataResult.Content.ReadAsStringAsync().Result;
                    listMtMenu = JsonConvert.DeserializeObject<List<MasterMenu>>(res);
                    for (int i = 0; i < listMtMenu.Count; i++)
                    {
                        if (listMtMenu[i].created_date != null) listMtMenu[i].created_date = listMtMenu[i].created_date.Replace("T00:00:00", "");
                        if (listMtMenu[i].changed_date != null) listMtMenu[i].changed_date = listMtMenu[i].changed_date.Replace("T00:00:00", "");

                        string action = "<button type='button' class='btn btn-sm btn-icon btn-icon rounded-circle bg-gradient-primary waves-effect waves-light' style='margin-right: 5px;' onclick=edit_menu('" + listMtMenu[i].id_menu + "')><i class='fas fa-edit'></i></button>" + "  " +
                                        "<button type='button' class='btn btn-sm btn-icon btn-icon rounded-circle bg-gradient-danger waves-effect waves-light' onclick=delete_menu('" + listMtMenu[i].id_menu + "')><i class='fa fa-trash-alt'></i></button>";

                        listMtMenuArr.Add(new MasterMenuArray()
                        {
                            id_menu = listMtMenu[i].id_menu,
                            id_parent = listMtMenu[i].id_parent,
                            nama_menu = listMtMenu[i].nama_menu,
                            link = listMtMenu[i].link,
                            icon = listMtMenu[i].icon,
                            created_by = listMtMenu[i].created_by,
                            created_date = listMtMenu[i].created_date,
                            changed_by = listMtMenu[i].changed_by,
                            changed_date = listMtMenu[i].changed_date,
                            IsParent = listMtMenu[i].IsParent,
                            MenuOrder = listMtMenu[i].MenuOrder,
                            MenuLevel = listMtMenu[i].MenuLevel,
                            action = action
                        });
                    }

                    metadata = new
                    {
                        Code = "200",
                        Message = "OK"
                    };
                    return Json(new { response = metadata, data = listMtMenuArr }, JsonRequestBehavior.AllowGet);
                }
            }

            metadata = new
            {
                Code = "400",
                Message = "Failed"
            };
            return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("GetDataByIdMenu")]
        public ActionResult GetDataByIdMenu(string id)
        {
            dynamic metadata = "";
            List<MasterMenu> listMtMenu = new List<MasterMenu>();
            using (var client = new HttpClient())
            {
                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                var getData = client.GetAsync("api/Menu/Get/" + id);
                getData.Wait();

                var dataResult = getData.Result;
                if (dataResult.IsSuccessStatusCode)
                {
                    var res = dataResult.Content.ReadAsStringAsync().Result;
                    listMtMenu = JsonConvert.DeserializeObject<List<MasterMenu>>(res);
                    for (int i = 0; i < listMtMenu.Count; i++)
                    {
                        if (listMtMenu[i].created_date != null) listMtMenu[i].created_date = listMtMenu[i].created_date.Replace("T00:00:00", "");
                        if (listMtMenu[i].changed_date != null) listMtMenu[i].changed_date = listMtMenu[i].changed_date.Replace("T00:00:00", "");
                    }

                    metadata = new
                    {
                        Code = "200",
                        Message = "OK"
                    };

                    return Json(new { response = metadata, data = listMtMenu }, JsonRequestBehavior.AllowGet);
                }
            }

            metadata = new
            {
                Code = "400",
                Message = "Failed"
            };

            return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(string id)
        {
            dynamic metadata = "";
            try
            {
                if(id != "")
                {
                    using (var client = new HttpClient())
                    {
                        var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                        var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                        var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                        client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(
                            new MediaTypeWithQualityHeaderValue("application/json"));

                        var body = new MasterMenu
                        {
                            id_menu = id
                        };
                        string jsonString = (Newtonsoft.Json.JsonConvert.SerializeObject(body));
                        var getData = client.PostAsync("api/Menu/DeleteMenu", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                        getData.Wait();

                        var dataResult = getData.Result;
                        if (dataResult.IsSuccessStatusCode)
                        {
                            metadata = new
                            {
                                Code = "200",
                                Message = "OK"
                            };
                            return Json( new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                metadata = new
                {
                    Code = "400",
                    Message = "Failed"
                };
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            } catch
            {
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddMenu()
        {
            dynamic metadata = "";
            try
            {
                string idMenu = Request.Form.GetValues("IDMenu").FirstOrDefault();
                string idParent = "0";
                try
                {
                    idParent = Request.Form.GetValues("IDParent").FirstOrDefault();
                }
                catch
                {
                    idParent = "0";
                }
                string namaMenu = Request.Form.GetValues("NamaMenu").FirstOrDefault();
                string link = Request.Form.GetValues("Link").FirstOrDefault();
                string icon = Request.Form.GetValues("Icon").FirstOrDefault();
                bool IsParent = bool.Parse(Request.Form.GetValues("IsParentValue").FirstOrDefault());
                var MenuOrder = Request.Form.GetValues("MenuOrder").FirstOrDefault();
                var MenuLevel = Request.Form.GetValues("MenuLevel").FirstOrDefault();
                string createdDate = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");

                List<MasterMenu> listMenu = new List<MasterMenu>();

                using (var client = new HttpClient())
                {
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);



                    //Cek apakah id sudah terdaftar
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String); ;
                    var getUser = client.GetAsync($"api/Menu/Get/{idMenu}");
                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        var Res = userResult.Content.ReadAsStringAsync().Result;
                        listMenu = JsonConvert.DeserializeObject<List<MasterMenu>>(Res);
                    }
                    else
                    {
                        metadata = new
                        {
                            code = "400",
                            message = "Terjadi kesalahan"
                        };
                        return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                    }

                    if (listMenu.Count > 0)
                    {
                        metadata = new
                        {
                            code = "401",
                            message = "ID Menu sudah terdaftar."
                        };
                        return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                    }


                    
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));

                    var body = new MasterMenu
                    {
                        id_menu = idMenu,
                        id_parent = idParent,
                        nama_menu = namaMenu,
                        link = link,
                        icon = icon,
                        created_by = user,
                        created_date = createdDate,
                        IsParent = IsParent,
                        MenuOrder = int.Parse(MenuOrder),
                        MenuLevel = int.Parse(MenuLevel),
                    };
                    string jsonString = (Newtonsoft.Json.JsonConvert.SerializeObject(body));
                    var getData = client.PostAsync("api/Menu/CreateMenu", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        metadata = new
                        {
                            Code = "200",
                            Message = "OK"
                        };
                        return Json(new { response = metadata, data = body }, JsonRequestBehavior.AllowGet);
                    }
                }

                metadata = new
                {
                    Code = "400",
                    Message = "Failed"
                };
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateMenu()
        {
            dynamic metadata = "";
            try
            {
                string idParent = "0";
                string idMenu = Request.Form.GetValues("IDMenu").FirstOrDefault();
                try
                {
                    idParent = Request.Form.GetValues("IDParent").FirstOrDefault();
                }
                catch
                {
                    idParent = "0";
                }
                
                string namaMenu = Request.Form.GetValues("NamaMenu").FirstOrDefault();
                string link = Request.Form.GetValues("Link").FirstOrDefault();
                string icon = Request.Form.GetValues("Icon").FirstOrDefault();
                bool IsParent = bool.Parse(Request.Form.GetValues("IsParentValue").FirstOrDefault());
                var MenuOrder = Request.Form.GetValues("MenuOrder").FirstOrDefault();
                var MenuLevel = Request.Form.GetValues("MenuLevel").FirstOrDefault();
                string changedDate = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");

                using (var client = new HttpClient())
                {
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));

                    var body = new MasterMenu
                    {
                        id_menu = idMenu,
                        id_parent = idParent,
                        nama_menu = namaMenu,
                        link = link,
                        icon = icon,
                        changed_by = user,
                        changed_date = changedDate,
                        IsParent = IsParent,
                        MenuOrder = int.Parse(MenuOrder),
                        MenuLevel = int.Parse(MenuLevel),
                    };
                    string jsonString = (Newtonsoft.Json.JsonConvert.SerializeObject(body));
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    var getData = client.PostAsync("api/Menu/UpdateMenu", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        metadata = new
                        {
                            Code = "200",
                            Message = "OK"
                        };
                        return Json(new { response = metadata, data = body }, JsonRequestBehavior.AllowGet);
                    }
                }

                metadata = new
                {
                    Code = "400",
                    Message = "Failed"
                };
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult getListParentMenu()
        {
            List<MasterMenu> listMenu = new List<MasterMenu>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync("api/Menu/Get");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listMenu = JsonConvert.DeserializeObject<List<MasterMenu>>(Res).Where(x=>x.IsParent == true).ToList();

                }
                return Json(new { data = listMenu, JsonRequestBehavior.AllowGet });
            }
        }

        [HttpPost]
        public ActionResult GetByID(string id)
        {
            dynamic metadata = "";
            if (id == null)
            {
                metadata = new
                {
                    code = "402",
                    message = "ID tidak boleh kosong."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }

            List<MasterMenu> listMenu = new List<MasterMenu>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync($"api/Menu/Get/{id}");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listMenu = JsonConvert.DeserializeObject<List<MasterMenu>>(Res);
                    if(listMenu.Count == 0)
                    {
                        metadata = new
                        {
                            code = "403",
                            message = "Menu ini tidak memiliki parent"
                        };
                        return Json(new { metaData = metadata, response = "403" }, JsonRequestBehavior.AllowGet);
                    }
                    if (listMenu != null)
                    {
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        return Json(new { metaData = metadata, response = listMenu }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        metadata = new
                        {
                            code = "400",
                            message = "Data Menu dengan ID tsb tidak ditemukan"
                        };
                        return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    metadata = new
                    {
                        code = "400",
                        message = "Terjadi kesalahan"
                    };
                    return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}