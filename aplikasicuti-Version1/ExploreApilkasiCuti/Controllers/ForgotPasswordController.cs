﻿using ExploreApilkasiCuti.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Mvc;

namespace ExploreApilkasiCuti.Controllers
{
    public class ForgotPasswordController : Controller
    {
        // GET: ForgotPassword
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AjaxResetPassword(dynamic body)
        {
            dynamic metadata = "";
            var Res = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    JObject myObject = JsonConvert.DeserializeObject<JObject>(Convert.ToString(body[0]));
                    var getUser = client.PostAsync("api/ForgotPassword/ResetPassword", new StringContent(JsonConvert.SerializeObject(myObject), Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    var userResult = getUser.Result;

                    if (userResult.IsSuccessStatusCode)
                    {
                        Res = userResult.Content.ReadAsStringAsync().Result;
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = Res
                        };
                        return Json(new { metaData = metadata, response = Res }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        Res = userResult.Content.ReadAsStringAsync().Result;
                        var result = JsonConvert.DeserializeObject<JToken>(Res);
                        Res = result["Message"].ToString();
                    }

                }
                // TODO: Add insert logic here

                //return RedirectToAction("Index");
                metadata = new
                {
                    code = "400",
                    message = Res
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Res = e.Message.ToString();
                metadata = new
                {
                    code = "400",
                    message = Res
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}