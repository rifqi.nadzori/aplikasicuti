﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ExploreApilkasiCuti.Models;
using Newtonsoft.Json;
using System.Web.Routing;
namespace ExploreApilkasiCuti.Controllers
{
    [NoDirectAccess]
    public class MasterJabatanController : Controller
    {
        // GET: MasterJabatan
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            string actionName = filterContext.ActionDescriptor.ActionName;
            if (session.IsNewSession || Session["Username"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {
                     { "controller", "LoginPage" },
                     { "action", "Index" }
                });
            }

        }

        public ActionResult Index()
        {

            var link = "/MasterJabatan";
            IEnumerable<MasterMenuArray> listDataMenuLink = GroupMenuRepository.Instance.getDatabyLink(link);
            ViewBag.Menulist = Session["menu_list"];
            if (listDataMenuLink != null)
            {
                foreach (var item in listDataMenuLink)
                {
                    ViewBag.ParentID = item.id_parent;
                    ViewBag.MenuID = item.id_menu;
                    ViewBag.NamaMenu = "Master Jabatan";
                    ViewBag.Menulist = Session["menu_list"];

                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Error404");
            }
        }
        /*
        [HttpGet]
        public ActionResult Index(MasterJabatan masterJabatan)
        {
            List<MasterJabatan> listMtJab = new List<MasterJabatan>();
            using (var client = new HttpClient())
            {
                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                var getData = client.GetAsync("api/Jabatan/Get");
                getData.Wait();

                var dataResult = getData.Result;
                if(dataResult.IsSuccessStatusCode)
                {
                    var res = dataResult.Content.ReadAsStringAsync().Result;
                    listMtJab = JsonConvert.DeserializeObject < List < MasterJabatan >> (res);
                    for(int i=0; i<listMtJab.Count; i++)
                    {
                        if(listMtJab[i].created_date != null) listMtJab[i].created_date = listMtJab[i].created_date.Replace("T00:00:00", "");
                        if(listMtJab[i].changed_date != null) listMtJab[i].changed_date = listMtJab[i].changed_date.Replace("T00:00:00", "");
                    }
                    return View(listMtJab);
                }
            }
            return View(listMtJab);
        }
        */
        [HttpPost, ActionName("LoadDataMasterJabatan")]
        public ActionResult LoadDataMasterJabatan()
        {
            dynamic metadata = "";
            List<MasterJabatan> listMtJab = new List<MasterJabatan>();
            List<MasterJabatanArray> listMtJabArr = new List<MasterJabatanArray>();
            using (var client = new HttpClient())
            {
                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                var getData = client.GetAsync("api/Jabatan/Get");
                getData.Wait();

                var dataResult = getData.Result;
                if (dataResult.IsSuccessStatusCode)
                {
                    var res = dataResult.Content.ReadAsStringAsync().Result;
                    listMtJab = JsonConvert.DeserializeObject<List<MasterJabatan>>(res);
                    for (int i = 0; i < listMtJab.Count; i++)
                    {
                        if (listMtJab[i].created_date != null) listMtJab[i].created_date = listMtJab[i].created_date.Replace("T00:00:00", "");
                        if (listMtJab[i].changed_date != null) listMtJab[i].changed_date = listMtJab[i].changed_date.Replace("T00:00:00", "");

                        string action = "<button type='button' class='btn btn-sm btn-icon btn-icon rounded-circle bg-gradient-primary waves-effect waves-light' style='margin-right: 5px;' onclick=edit_jabatan('" + listMtJab[i].id_jabatan + "')><i class='fas fa-edit'></i></button>" + "  " +
                                        "<button type='button' class='btn btn-sm btn-icon btn-icon rounded-circle bg-gradient-danger waves-effect waves-light' onclick=delete_jabatan('" + listMtJab[i].id_jabatan + "')><i class='fa fa-trash-alt'></i></button>";

                        listMtJabArr.Add(new MasterJabatanArray()
                        {
                            id_jabatan = listMtJab[i].id_jabatan,
                            nama_jabatan = listMtJab[i].nama_jabatan,
                            id_saldo_ulang_bulan = listMtJab[i].id_saldo_ulang_bulan,
                            created_by = listMtJab[i].created_by,
                            created_date = listMtJab[i].created_date,
                            changed_by = listMtJab[i].changed_by,
                            changed_date = listMtJab[i].changed_date,
                            action = action
                        });
                    }

                    metadata = new
                    {
                        Code = "200",
                        Message = "OK"
                    };
                    return Json(new { response = metadata, data = listMtJabArr }, JsonRequestBehavior.AllowGet);
                }
            }

            metadata = new
            {
                Code = "400",
                Message = "Failed"
            };
            return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("GetDataById")]
        public ActionResult GetDataById(string id)
        {
            dynamic metadata = "";
            List<MasterJabatan> listMtJab = new List<MasterJabatan>();
            using (var client = new HttpClient())
            {
                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                var getData = client.GetAsync("api/Jabatan/Get/" + id);
                getData.Wait();

                var dataResult = getData.Result;
                if (dataResult.IsSuccessStatusCode)
                {
                    var res = dataResult.Content.ReadAsStringAsync().Result;
                    listMtJab = JsonConvert.DeserializeObject<List<MasterJabatan>>(res);
                    for (int i = 0; i < listMtJab.Count; i++)
                    {
                        if (listMtJab[i].created_date != null) listMtJab[i].created_date = listMtJab[i].created_date.Replace("T00:00:00", "");
                        if (listMtJab[i].changed_date != null) listMtJab[i].changed_date = listMtJab[i].changed_date.Replace("T00:00:00", "");
                    }

                    metadata = new
                    {
                        Code = "200",
                        Message = "OK"
                    };

                    return Json(new { response = metadata, data = listMtJab }, JsonRequestBehavior.AllowGet);
                }
            }

            metadata = new
            {
                Code = "400",
                Message = "Failed"
            };

            return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(string id)
        {
            dynamic metadata = "";
            try
            {
                if(id != "")
                {
                    using (var client = new HttpClient())
                    {
                        var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                        var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                        var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                        client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(
                            new MediaTypeWithQualityHeaderValue("application/json"));

                        var body = new MasterJabatan
                        {
                            id_jabatan = id
                        };
                        string jsonString = (Newtonsoft.Json.JsonConvert.SerializeObject(body));
                        var getData = client.PostAsync("api/Jabatan/DeleteJabatan", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                        getData.Wait();

                        var dataResult = getData.Result;
                        if (dataResult.IsSuccessStatusCode)
                        {
                            metadata = new
                            {
                                Code = "200",
                                Message = "OK"
                            };
                            return Json( new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                metadata = new
                {
                    Code = "400",
                    Message = "Failed"
                };
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            } catch
            {
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddJabatan()
        {
            dynamic metadata = "";
            try
            {
                string idJabatan = Request.Form.GetValues("IDJabatan").FirstOrDefault();
                string namaJabatan = Request.Form.GetValues("NamaJabatan").FirstOrDefault();
                string idSaldoUlangBulan = Request.Form.GetValues("CutiBulanan").FirstOrDefault();
                string createdDate = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");

                using (var client = new HttpClient())
                {
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));

                    var body = new MasterJabatan
                    {
                        id_jabatan = idJabatan,
                        nama_jabatan = namaJabatan,
                        id_saldo_ulang_bulan = idSaldoUlangBulan,
                        created_by = user,
                        created_date = createdDate
                    };
                    string jsonString = (Newtonsoft.Json.JsonConvert.SerializeObject(body));
                    var getData = client.PostAsync("api/Jabatan/CreateJabatan", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        metadata = new
                        {
                            Code = "200",
                            Message = "OK"
                        };
                        return Json(new { response = metadata, data = body }, JsonRequestBehavior.AllowGet);
                    }
                }

                metadata = new
                {
                    Code = "400",
                    Message = "Failed"
                };
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateJabatan()
        {
            dynamic metadata = "";
            try
            {
                string idJabatan = Request.Form.GetValues("IDJabatan").FirstOrDefault();
                string namaJabatan = Request.Form.GetValues("NamaJabatan").FirstOrDefault();
                string idSaldoUlangBulan = Request.Form.GetValues("CutiBulanan").FirstOrDefault();
                string changedDate = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");

                using (var client = new HttpClient())
                {
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));

                    var body = new MasterJabatan
                    {
                        id_jabatan = idJabatan,
                        nama_jabatan = namaJabatan,
                        id_saldo_ulang_bulan = idSaldoUlangBulan,
                        changed_by = user,
                        changed_date = changedDate
                    };
                    string jsonString = (Newtonsoft.Json.JsonConvert.SerializeObject(body));
                    var getData = client.PostAsync("api/Jabatan/UpdateJabatan", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        metadata = new
                        {
                            Code = "200",
                            Message = "OK"
                        };
                        return Json(new { response = metadata, data = body }, JsonRequestBehavior.AllowGet);
                    }
                }

                metadata = new
                {
                    Code = "400",
                    Message = "Failed"
                };
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { response = metadata, data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, ActionName("GetDataJabatan")]
        public ActionResult GetDataJabatan()
        {
            List<MasterJabatan> listJabatan = new List<MasterJabatan>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync("api/Jabatan/Get");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listJabatan = JsonConvert.DeserializeObject<List<MasterJabatan>>(Res);

                }
                return Json(new { data = listJabatan, JsonRequestBehavior.AllowGet });
                //return Json(listJabatan, JsonRequestBehavior.AllowGet);
            }
        }
    }
}