﻿using ExploreApilkasiCuti.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ExploreApilkasiCuti.Controllers
{
    [NoDirectAccess]
    public class GroupAccessController : Controller
    {
        // GET: GroupAccess
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            string actionName = filterContext.ActionDescriptor.ActionName;
            if (session.IsNewSession || Session["Username"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {
                     { "controller", "LoginPage" },
                     { "action", "Index" }
                });
            }

        }

        public ActionResult Index()
        {
            
            var link = "/GroupAccess";
            IEnumerable<MasterMenuArray> listDataMenuLink = GroupMenuRepository.Instance.getDatabyLink(link);
            ViewBag.Menulist = Session["menu_list"];
            if (listDataMenuLink!= null)
            {
                foreach (var item in listDataMenuLink)
                {
                    ViewBag.ParentID = item.id_parent;
                    ViewBag.MenuID = item.id_menu;
                    ViewBag.NamaMenu = "Group Access";
                    ViewBag.Menulist = Session["menu_list"];

                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Error404");
            }
        }

        [HttpPost, ActionName("GetDataMasterGroup")]
        public ActionResult GetDataMasterGroup()
        {
            List<MasterGroup> listGroup = new List<MasterGroup>();


            List<dynamic> _listval = MasterGroupRepository.Instance.getDataGroup();
            if (_listval[0] == "Success") listGroup = _listval[1];
            return Json(new { data = listGroup, JsonRequestBehavior.AllowGet });
        }

        [HttpPost, ActionName("GetDataByID")]
        public ActionResult GetDataByID(string id)
        {
            dynamic metadata = "";
            List<dynamic> _listval;
            List<MasterGroup> listGroup = new List<MasterGroup>();
            if (id == null)
            {
                metadata = new
                {
                    code = "402",
                    message = "ID Group tidak boleh kosong."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }

            _listval = MasterGroupRepository.Instance.getDataByID(id);
            if (_listval[0].ToString() == "Success")
            {
                listGroup = _listval[1];
                metadata = new
                {
                    code = "200",
                    message = "OK"
                };
                return Json(new { metaData = metadata, response = listGroup }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                metadata = new
                {
                    code = "400",
                    message = _listval[1].ToString()
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: GroupAccess/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: GroupAccess/Create
        public ActionResult Create()
        {
            return View();
        }

       

        [HttpPost]
        //public ActionResult Create(FormCollection collection)
        public ActionResult Create(MasterGroupListKaryawan group)
        {
            dynamic metadata = "";
            bool valid = true;
            List<dynamic> _listval = new List<dynamic>();
            List<MasterGroup> listGroup = new List<MasterGroup>();
            try
            {
                //Cek apakah sudah ada id group yg sama
                _listval = MasterGroupRepository.Instance.getDataByID(group.masterGroup.id_group);
                if (_listval[0] == "Success")
                {
                    listGroup = _listval[1];
                    if (listGroup.Count > 0) valid = false;
                }
                else
                {
                    metadata = new
                    {
                        code = "400",
                        message = "Terjadi kesalahan"
                    };
                    return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                }

                // Jika tidak ada id group yg sama, maka lanjut
                if (valid)
                {
                    _listval = MasterGroupRepository.Instance.CreateGroup(group.masterGroup);
                    if (_listval[0] == "Success")
                    {
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        return Json(new { metaData = metadata, response = group }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        metadata = new
                        {
                            code = "400",
                            message = "Gagal menyimpan data. " + _listval[1],
                        };
                        return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    metadata = new
                    {
                        code = "401",
                        message = "Data dengan ID Group yang sama telah terdaftar."
                    };
                    return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception e)
            {

                metadata = new
                {
                    code = "400",
                    message = "Gagal menyimpan data. " + e.ToString(),
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Update(MasterGroupListKaryawan group)
        {
            dynamic metadata = ""; string jsonString = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                   
                    var body = new MasterGroup
                    {
                        id_group = group.masterGroup.id_group,
                        nama_group = group.masterGroup.nama_group,
                        changed_by = user,
                        changed_date = DateTime.Now
                    };
                    jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    var getUser = client.PostAsync("api/Group/UpdateGroup", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        return Json(new { metaData = metadata, response = group }, JsonRequestBehavior.AllowGet);
                    }

                }
                // TODO: Add insert logic here

                //return RedirectToAction("Index");
                metadata = new
                {
                    code = "400",
                    message = "Failed to Update Data. " + jsonString
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                metadata = new
                {
                    code = "400",
                    message = "Failed to Update Data." + jsonString
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        // GET: GroupAccess/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: GroupAccess/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: GroupAccess/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: GroupAccess/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult GetTreview(string AccountGroupID)
        {
            dynamic metadata = "";
            string _treeview = GroupMenuRepository.Instance.getTreeView(AccountGroupID);

            return Json(_treeview, JsonRequestBehavior.AllowGet);
        }
        public ActionResult saveRole()
        {
            string[] MenuID = Request.Form.GetValues("id_menu[]");
            var GroupID = Request.Form.GetValues("AccountGroupID").FirstOrDefault();
            List<dynamic> valResult = GroupMenuRepository.Instance.saveGroupMenu(MenuID, GroupID);
            string _val = "";
            if (valResult[0] == "Success")
            {
                _val = "Success";
            }
            else
            {
                _val = valResult[1];
            }
            return Json(valResult, JsonRequestBehavior.AllowGet);
        }
        

        [HttpPost, ActionName("DeleteGroup")]
        public ActionResult DeleteGroup(string id)
        {
            List<dynamic> _listval = new List<dynamic>();
            dynamic metadata = "";
            try
            {
                _listval = MasterGroupRepository.Instance.DeleteGroup(id);
                if (_listval[0] == "Success")
                {
                    metadata = new
                    {
                        code = "200",
                        message = "Success. Berhasil Menghapus Data"
                    };
                    return Json(new { metaData = metadata, response = $"Data {id} sukses dihapus" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    metadata = new
                    {
                        code = "400",
                        message = "Gagal Menghapus Data"
                    };
                    return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                metadata = new
                {
                    code = "400",
                    message = "Gagal Menghapus Data"
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, ActionName("GetDataGroup")]
        public ActionResult GetDataGroup()
        {
            List<MasterGroup> listGroup = new List<MasterGroup>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync("api/Group/Get");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listGroup = JsonConvert.DeserializeObject<List<MasterGroup>>(Res);

                }
                return Json(new { data = listGroup, JsonRequestBehavior.AllowGet });
                //return Json(listGroup, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
