﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using ExploreApilkasiCuti.Models;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Script.Serialization;
using System.Net;
using System.Web.Routing;
//using System.Net.Http.Headers;

namespace ExploreApilkasiCuti.Controllers
{
    [NoDirectAccess]
    public class MasterJenisPengajuanController : Controller
    {
        // GET: MasterJabatan
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            string actionName = filterContext.ActionDescriptor.ActionName;
            if (session.IsNewSession || Session["Username"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {
                     { "controller", "LoginPage" },
                     { "action", "Index" }
                });
            }

        }

        public ActionResult Index()
        {

            var link = "/MasterJenisPengajuan";
            IEnumerable<MasterMenuArray> listDataMenuLink = GroupMenuRepository.Instance.getDatabyLink(link);
            ViewBag.Menulist = Session["menu_list"];
            if (listDataMenuLink != null)
            {
                foreach (var item in listDataMenuLink)
                {
                    ViewBag.ParentID = item.id_parent;
                    ViewBag.MenuID = item.id_menu;
                    ViewBag.NamaMenu = "Master Jenis Pengajuan";
                    ViewBag.Menulist = Session["menu_list"];

                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Error404");
            }
        }
        /*
        [HttpGet]
        public ActionResult Index(MasterJenisPengajuan jp)
        {
            List<MasterJenisPengajuan> listjp = new List<MasterJenisPengajuan>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                var getData = client.GetAsync("api/JenisPengajuan/Get");
                getData.Wait();

                var dataResult = getData.Result;
                if (dataResult.IsSuccessStatusCode)
                {
                    var res = dataResult.Content.ReadAsStringAsync().Result;
                    listjp = JsonConvert.DeserializeObject<List<MasterJenisPengajuan>>(res);
                    return View(listjp);
                }
            }
            return View(listjp);
        }
        */
        // GET: Karyawan/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Karyawan/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Karyawan/Create
        [HttpPost]
        public ActionResult Create(MasterJenisPengajuan jp)
        {
            dynamic metadata = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var body = new MasterJenisPengajuan
                    {
                        id_jenis_pengajuan = jp.id_jenis_pengajuan,
                        nama_pengajuan = jp.nama_pengajuan,
                        durasi = jp.durasi,
                        created_by = user,
                        created_date = DateTime.Now

                    };
                    string jsonString = (Newtonsoft.Json.JsonConvert.SerializeObject(body));
                    var getUser = client.PostAsync("api/JenisPengajuan/CreateJenisPengajuan", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        return Json(new { metaData = metadata, response = jp }, JsonRequestBehavior.AllowGet);
                    }

                }
                // TODO: Add insert logic here

                metadata = new
                {
                    code = "400",
                    message = "Failed to Add New Row, Write Different ID."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, ActionName("GetDatatableAll")]
        public ActionResult GetDatatableAll()
        {
            List<MasterJenisPengajuan> listjp = new List<MasterJenisPengajuan>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync("api/JenisPengajuan/Get");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listjp = JsonConvert.DeserializeObject<List<MasterJenisPengajuan>>(Res);

                }
                return Json(new { data = listjp, JsonRequestBehavior.AllowGet });
            }
        }

        [HttpPost, ActionName("GetDataByID")]
        public ActionResult GetDataByID(string id)
        {
            dynamic metadata = "";
            if (id == null)
            {
                metadata = new
                {
                    code = "402",
                    message = "ID tidak boleh kosong."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }

            List<MasterJenisPengajuan> listjp = new List<MasterJenisPengajuan>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync($"api/JenisPengajuan/Get/{id}");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listjp = JsonConvert.DeserializeObject<List<MasterJenisPengajuan>>(Res);
                    if (listjp != null)
                    {
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        return Json(new { metaData = metadata, response = listjp }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        metadata = new
                        {
                            code = "400",
                            message = "Data Jenis Pengajuan dengan ID tsb tidak ditemukan"
                        };
                        return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    metadata = new
                    {
                        code = "400",
                        message = "Terjadi kesalahan"
                    };
                    return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        //public ActionResult Create(FormCollection collection)
        public ActionResult Update(MasterJenisPengajuan jp)
        {
            dynamic metadata = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    //client.DefaultRequestHeaders.Accept.Clear();
                    //client.DefaultRequestHeaders.Accept.Add(
                    //    new MediaTypeWithQualityHeaderValue("application/json"));

                    var body = new MasterJenisPengajuan
                    {
                        id_jenis_pengajuan = jp.id_jenis_pengajuan,
                        nama_pengajuan = jp.nama_pengajuan,
                        durasi = jp.durasi,
                        created_by = user,
                        created_date = DateTime.Now
                    };
                    string jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    var getUser = client.PostAsync("api/JenisPengajuan/UpdateJenisPengajuan", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        return Json(new { metaData = metadata, response = jp }, JsonRequestBehavior.AllowGet);
                    }

                }
                // TODO: Add insert logic here

                //return RedirectToAction("Index");
                metadata = new
                {
                    code = "400",
                    message = "Failed to Update Data Karyawan."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                metadata = new
                {
                    code = "400",
                    message = "Failed to Update Data Karyawan."
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: MasterPengajuan/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<MasterJenisPengajuan> listjp = new List<MasterJenisPengajuan>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                //client.DefaultRequestHeaders.Add("Accept", "*/*");
                var getUser = client.GetAsync($"api/JenisPengajuan/Get/{id}");
                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    listjp = JsonConvert.DeserializeObject<List<MasterJenisPengajuan>>(Res);


                }
            }
            if (listjp == null)
            {
                return HttpNotFound();
            }
            return View(listjp);
        }
        // POST: MasterPengajuan/Delete/{id_karyawan}
        [HttpPost, ActionName("DeleteJenisPengajuan")]
        public ActionResult DeleteJenisPengajuan(string id)
        {
            dynamic metadata = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    //client.DefaultRequestHeaders.Accept.Clear();
                    //client.DefaultRequestHeaders.Accept.Add(
                    //    new MediaTypeWithQualityHeaderValue("application/json"));

                    var body = new MasterJenisPengajuan
                    {
                        id_jenis_pengajuan = id
                    };
                    string jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    var getUser = client.PostAsync("api/JenisPengajuan/DeleteJenisPengajuan", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "Success. Berhasil Menghapus Data"
                        };
                        return Json(new { metaData = metadata, response = body }, JsonRequestBehavior.AllowGet);
                    }

                }
                // TODO: Add insert logic here

                //return RedirectToAction("Index");
                metadata = new
                {
                    code = "400",
                    message = "Gagal Menghapus Data"
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                metadata = new
                {
                    code = "400",
                    message = "Gagal Menghapus Data"
                };
                return Json(new { metaData = metadata, response = "" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}