﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExploreApilkasiCuti.Models
{
    public class MasterGroup
    {
        public string id_group { get; set; }
        public string nama_group { get; set; }
        public string created_by { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public string changed_by { get; set; }
        public Nullable<System.DateTime> changed_date { get; set; }
    }

    public class MasterGroupListKaryawan
    {
        public MasterGroup masterGroup { get; set; }
        
    }
}