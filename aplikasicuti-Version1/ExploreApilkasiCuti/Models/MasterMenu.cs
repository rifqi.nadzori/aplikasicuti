﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExploreApilkasiCuti.Models
{
    public class MasterMenu
    {
        public string id_menu { get; set; }
        public string id_parent { get; set; }
        public string nama_menu { get; set; }
        public string link { get; set; }
        public string icon { get; set; }
        public string created_by { get; set; }
        public string created_date { get; set; }
        public string changed_by { get; set; }
        public string changed_date { get; set; }
        public bool IsParent { get; set; }
        public int MenuOrder { get; set; }
        public int MenuLevel { get; set; }
    }

    public class MasterMenuArray
    {
        public string id_menu { get; set; }
        public string id_parent { get; set; }
        public string nama_menu { get; set; }
        public string link { get; set; }
        public string icon { get; set; }
        public string created_by { get; set; }
        public string created_date { get; set; }
        public string changed_by { get; set; }
        public string changed_date { get; set; }
        public bool IsParent { get; set; }
        public int MenuOrder { get; set; }
        public int MenuLevel { get; set; }
        public string action { get; set; }
    }
}