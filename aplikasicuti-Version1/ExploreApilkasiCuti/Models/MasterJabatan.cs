﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExploreApilkasiCuti.Models
{
    public class MasterJabatan
    {
        /*public string namaJabatan { get; set; }
        public string idSaldo { get; set; }
        public string createBy { get; set; }
        public string createDate { get; set; }
        public string changeBy { get; set; }
        public string changeDate { get; set; }*/
        public string id_jabatan { get; set; }
        public string nama_jabatan { get; set; }
        public string id_saldo_ulang_bulan { get; set; }
        public string created_by { get; set; }
        public string created_date { get; set; }
        public string changed_by { get; set; }
        public string changed_date { get; set; }
        /*public int TransactionID { get; set; }
        public string NamaBarang { get; set; }
        public int Harga { get; set; }
        public int OrderQty { get; set; }
        public int TotalPrice { get; set; }*/
        //TEST
    }

    public class MasterJabatanArray
    {
        /*public string namaJabatan { get; set; }
        public string idSaldo { get; set; }
        public string createBy { get; set; }
        public string createDate { get; set; }
        public string changeBy { get; set; }
        public string changeDate { get; set; }*/
        public string id_jabatan { get; set; }
        public string nama_jabatan { get; set; }
        public string id_saldo_ulang_bulan { get; set; }
        public string created_by { get; set; }
        public string created_date { get; set; }
        public string changed_by { get; set; }
        public string changed_date { get; set; }
        public string action { get; set; }
        /*public int TransactionID { get; set; }
        public string NamaBarang { get; set; }
        public int Harga { get; set; }
        public int OrderQty { get; set; }
        public int TotalPrice { get; set; }*/
        //TEST
    }
}