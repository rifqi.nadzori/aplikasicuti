﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ExploreApilkasiCuti.Models
{
    public class PengajuanDOL
    {
        public string id_pengajuan { get; set; }
        public DateTime tgl_pengajuan { get; set; }
        public string id_karyawan { get; set; }
        public string id_jenis_pengajuan { get; set; }
        public string nama_pengajuan { get; set; }
        public Nullable<float> durasi { get; set; }
        public Nullable<bool> denda_cuti { get; set; }
        public string alamat_cuti { get; set; }
        public string notelp_cuti { get; set; }
        public string keterangan_pengajuan { get; set; }
        public string approve_status { get; set; }
        public string keterangan_approve { get; set; }
        public string created_by { get; set; }
        public DateTime? created_date { get; set; }
        public string changed_by { get; set; }
        public DateTime? changed_date { get; set; }
        public string nama_approval { get; set; }
        public string nama_karyawan_approval { get; set; }
        public string id_approval { get; set; }
        public string approved_by { get; set; }
        public string nama_file { get; set; }
        public string link_file { get; set; }
        public string nama { get; set; }
        public List<PengajuanDetailDOL> details { get; set; }
    }

    public class PengajuanDOLArray
    {
        public string id_pengajuan { get; set; }
        public string tgl_pengajuan { get; set; }
        public string id_karyawan { get; set; }
        public string id_jenis_pengajuan { get; set; }
        public string nama_pengajuan { get; set; }
        public Nullable<float> durasi { get; set; }
        public Nullable<bool> denda_cuti { get; set; }
        public string alamat_cuti { get; set; }
        public string notelp_cuti { get; set; }
        public string keterangan_pengajuan { get; set; }
        public string approve_status { get; set; }
        public string keterangan_approve { get; set; }
        public string created_by { get; set; }
        public DateTime? created_date { get; set; }
        public string changed_by { get; set; }
        public DateTime? changed_date { get; set; }
        public string nama_approval { get; set; }
        public string nama_karyawan_approval { get; set; }
        public string id_approval { get; set; }
        public string approved_by { get; set; }
        public string action { get; set; }
        public string nama_file { get; set; }
        public string link_file { get; set; }
        public string nama { get; set; }
        public List<PengajuanDetailDOL> details { get; set; }
    }
}