﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExploreApilkasiCuti.Models
{
    public class MasterSaldoUlangBulan
    {
        public string id_saldo_ulang_bulan { get; set; }
        public string jml_cuti_per_bulan { get; set; }
        public string created_by { get; set; }
        public Nullable<DateTime> created_date { get; set; }
        public string changed_by { get; set; }
        public Nullable<DateTime> changed_date { get; set; }
    }
}