﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExploreApilkasiCuti.Models
{
    public class GroupMenu
    {
        public string id_group_menu { get; set; }
        public string id_group { get; set; }
        public string id_menu { get; set; }
        public string created_by { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        
    }
    public class GroupMenuArray
    {
        public string id_group_menu { get; set; }
        public string id_group { get; set; }
        public string id_menu { get; set; }
        public string created_by { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }

    }
}