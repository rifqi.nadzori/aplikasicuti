﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace ExploreApilkasiCuti.Models
{
    public class MasterGroupRepository
    {
        private static MasterGroupRepository instance = null;
        public static MasterGroupRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MasterGroupRepository();
                }

                return instance;
            }
        }

        public List<dynamic> getDataGroup()
        {
            List<dynamic> listval = new List<dynamic>();
            List<MasterGroup> listGroup = new List<MasterGroup>();
            
            try
            {

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    var getData = client.GetAsync($"api/Group/Get");
                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        var Res = dataResult.Content.ReadAsStringAsync().Result;
                        listGroup = JsonConvert.DeserializeObject<List<MasterGroup>>(Res);
                        if (listGroup.Count > 0)
                        {
                            listval.Add("Success");
                            listval.Add(listGroup);
                            return listval;
                        }
                        else
                        {
                            listval.Add("Not Success");
                            listval.Add($"Data tidak ditemukan");
                            return listval;
                        }

                    }
                    else
                    {
                        listval.Add("Not Success");
                        listval.Add(dataResult.StatusCode + " " + dataResult.ReasonPhrase);
                        return listval;
                    }
                }
            }
            catch (Exception e)
            {
                listval.Add("Not Success");
                listval.Add(e.ToString());
                return listval;
            }

        }
        public List<dynamic> getDataByID(string id)
        {
            List<dynamic> listval = new List<dynamic>();
            List<MasterGroup> listGroup = new List<MasterGroup>();

            try
            {

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    var getData = client.GetAsync($"api/Group/Get/{id}");
                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        var Res = dataResult.Content.ReadAsStringAsync().Result;
                        listGroup = JsonConvert.DeserializeObject<List<MasterGroup>>(Res);
                        listval.Add("Success");
                        listval.Add(listGroup);
                        return listval;
                    }
                    else
                    {
                        listval.Add("Not Success");
                        listval.Add(dataResult.StatusCode + " " + dataResult.ReasonPhrase);
                        return listval;
                    }
                }
            }
            catch (Exception e)
            {
                listval.Add("Not Success");
                listval.Add(e.ToString());
                return listval;
            }

        }

        public List<dynamic> CreateGroup(MasterGroup group)
        {
            List<dynamic> listval = new List<dynamic>();
            List<MasterGroup> listGroup = new List<MasterGroup>();

            try
            {

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    var body = new MasterGroup
                    {
                        id_group = group.id_group,
                        nama_group = group.nama_group,
                        created_by = user,
                        created_date = DateTime.Now
                    };
                    string jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    var postData = client.PostAsync("api/Group/CreateGroup", new StringContent(jsonString, Encoding.UTF8, "application/json"));
                    postData.Wait();

                    var dataResult = postData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        listval.Add("Success");
                        listval.Add("Data berhasil ditambahkan");
                        return listval;

                    }
                    else
                    {
                        listval.Add("Not Success");
                        listval.Add(dataResult.StatusCode + " " + dataResult.ReasonPhrase);
                        return listval;
                    }
                }
            }
            catch (Exception e)
            {
                listval.Add("Not Success");
                listval.Add(e.ToString());
                return listval;
            }

        }

        public List<dynamic> DeleteGroup(string id)
        {
            List<dynamic> _listval = new List<dynamic>();
            dynamic metadata = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    var body = new MasterGroup
                    {
                        id_group = id
                    };
                    string jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    var postData = client.PostAsync("api/Group/DeleteGroup", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    postData.Wait();

                    var dataResult = postData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        _listval.Add("Success");
                    }
                    else
                    {
                        _listval.Add("Not Success");
                        _listval.Add(dataResult.ReasonPhrase);
                    }
                    return _listval;
                }
            }
            catch(Exception e)
            {
                _listval.Add("Not Success");
                _listval.Add(e.ToString());
                return _listval;
            }

        }
    }
}