﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExploreApilkasiCuti.Models
{
    public class PengajuanCuti
    {
        public string id_pengajuan { get; set; }
        public string id_karyawan { get; set; }
        public string id_jenis_pengajuan { get; set; }
        public string nama_pengajuan { get; set; }
        public Nullable<DateTime> tgl_pengajuan { get; set; }
        public decimal durasi { get; set; }
        public Nullable<bool> denda_cuti { get; set; }
        public string alamat_cuti { get; set; }
        public string notelp_cuti { get; set; }
        public string keterangan_pengajuan { get; set; }
        public string approve_status { get; set; }
        public string keterangan_approve { get; set; }
        public string created_by { get; set; }
        public Nullable<DateTime> created_date { get; set; }
        public string changed_by { get; set; }
        public Nullable<DateTime> changed_date { get; set; }
        public string nama_approval { get; set; }
        public string nama_karyawan_approval { get; set; }
        public string id_approval { get; set; }
        public string approved_by { get; set; }
        public string nama_file { get; set; }
        public string link_file { get; set; }

        public string nama { get; set; }
        public List<PengajuanCutiDetailArray> details { get; set;}
    }

    public class PengajuanCutiArray
    {
        public int no_urut { get; set; }
        public string id_pengajuan { get; set; }
        public string id_karyawan { get; set; }
        public string id_jenis_pengajuan { get; set; }
        public string nama_pengajuan { get; set; }
        public string tgl_pengajuan { get; set; }
        public decimal durasi { get; set; }
        public Nullable<bool> denda_cuti { get; set; }
        public string alamat_cuti { get; set; }
        public string notelp_cuti { get; set; }
        public string keterangan_pengajuan { get; set; }
        public string approve_status { get; set; }
        public string keterangan_approve { get; set; }
        public string created_by { get; set; }
        public string created_date { get; set; }
        public string changed_by { get; set; }
        public string changed_date { get; set; }
        public string action { get; set; }
        public string nama_approval { get; set; }
        public string nama_karyawan_approval { get; set; }
        public string id_approval { get; set; }
        public string approved_by { get; set; }
        public string nama_file { get; set; }
        public string link_file { get; set; }

        public string nama { get; set; }
        public List<PengajuanCutiDetailArray> details { get; set; }
    }

    public class PengajuanCutiDetail
    {
        public string id_pengajuan { get; set; }
        public string id_pengajuan_detail { get; set; }
        public Nullable<DateTime> start_date { get; set; }
        public Nullable<DateTime> end_date { get; set; }
        public decimal durasi { get; set; }
    }

    public class PengajuanCutiDetailArray
    {
        public int no { get; set; }
        public string id_pengajuan { get; set; }
        public string id_pengajuan_detail { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
        public decimal durasi { get; set; }
        public string action { get; set; }
    }

    public class NewPengajuanCuti
    {
        public string id_pengajuan { get; set; }
        public string id_karyawan { get; set; }
        public string id_jenis_pengajuan { get; set; }
        public Nullable<DateTime> tgl_pengajuan { get; set; }
        public decimal durasi { get; set; }
        public bool denda_cuti { get; set; }
        public string alamat_cuti { get; set; }
        public string notelp_cuti { get; set; }
        public string keterangan_pengajuan { get; set; }
        public string approve_status { get; set; }
        public string keterangan_approve { get; set; }
        public string created_by { get; set; }
        public Nullable<DateTime> created_date { get; set; }
        public string changed_by { get; set; }
        public Nullable<DateTime> changed_date { get; set; }
        public string id_approval { get; set; }
        public string approved_by { get; set; }
        public List<PengajuanCutiDetail> details { get; set; }
    }
}