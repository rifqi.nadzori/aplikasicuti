﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;


namespace ExploreApilkasiCuti.Models
{
    public class LoginPageRepository
    {
        private static LoginPageRepository instance = null;
        public static LoginPageRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new LoginPageRepository();
                }

                return instance;
            }
        }
    }
}