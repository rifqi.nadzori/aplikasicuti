﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Security.Cryptography;


namespace ExploreApilkasiCuti.Models
{
    public class MasterKaryawanRepository
    {
        private static MasterKaryawanRepository instance = null;
        public static MasterKaryawanRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MasterKaryawanRepository();
                }

                return instance;
            }
        }
        public List<dynamic> getDataMasterKaryawan(string _idJabatan, string _startJDate, string _endJDate, string _status)
        {

            List<dynamic> listval = new List<dynamic>();
            List<MasterKaryawan> listKaryawan = new List<MasterKaryawan>();
            List<MasterKaryawanArray> listKrArray = new List<MasterKaryawanArray>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    string _url = "api/Karyawan/GetKaryawanJabatan?";
                    if (_idJabatan != "" && _idJabatan != null) _url += $"idjabatan={_idJabatan}&";
                    if (_startJDate != "" && _startJDate != null) _url += $"startjoindate={_startJDate}&";
                    if (_endJDate != "" && _endJDate != null) _url += $"endjoindate={_endJDate}&";
                    if (_status != "" && _status != null) _url += $"status={_status}";

                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    
                    var getData = client.GetAsync(_url);
                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        var Res = dataResult.Content.ReadAsStringAsync().Result;
                        listKaryawan = JsonConvert.DeserializeObject<List<MasterKaryawan>>(Res);

                        foreach (var item in listKaryawan)
                        {
                            MasterKaryawanArray kr = new MasterKaryawanArray();
                            kr.id_karyawan = item.id_karyawan;
                            kr.nama = item.nama;
                            kr.alamat = item.alamat;
                            kr.no_telp = item.no_telp;
                            kr.email = item.email;
                            kr.password = item.password;
                            kr.join_date = item.join_date.ToString("MM-dd-yyyy");
                            kr.id_jabatan = item.id_jabatan;
                            kr.nama_jabatan = item.nama_jabatan;
                            kr.status = item.status;
                            kr.created_by = item.created_by;
                            kr.created_date = item.created_date;
                            kr.changed_by = item.changed_by;
                            kr.changed_date = item.changed_date;
                            kr.id_group = item.id_group;
                            listKrArray.Add(kr);
                        }
                        listval.Add("Success");
                        listval.Add(listKrArray);
                        return listval;

                    }
                    listval.Add("Not Success");
                    listval.Add(dataResult.StatusCode + " " + dataResult.ReasonPhrase);
                    return listval;
                }
            }
            catch(Exception e)
            {
                listval.Add("Not Success");
                listval.Add(e.ToString());
                return listval;
            }
            
        }

        public List<dynamic> getDataMasterKaryawanByID(string id)
        {
            List<dynamic> listval = new List<dynamic>();
            List<MasterKaryawan> listKaryawan = new List<MasterKaryawan>();
            List<MasterKaryawanArray> listKrArray = new List<MasterKaryawanArray>();
            try
            {
                
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    var getData = client.GetAsync($"api/Karyawan/Get/{id}");
                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        var Res = dataResult.Content.ReadAsStringAsync().Result;
                        listKaryawan = JsonConvert.DeserializeObject<List<MasterKaryawan>>(Res);
                        if (listKaryawan.Count > 0)
                        {
                            // Isi nama_jabatan dengan memanggil api jabatan

                            List<dynamic> listvalJabatan = MasterJabatanRepositoryF.Instance.getMasterJabatanbyID(listKaryawan[0].id_jabatan);
                            if (listvalJabatan[0].ToString() == "Success")
                            {
                                List<MasterJabatan> listJabatan = listvalJabatan[1];
                                
                                MasterKaryawanArray kr = new MasterKaryawanArray();
                                var item = listKaryawan[0];
                                kr.id_karyawan = item.id_karyawan;
                                kr.nama = item.nama;
                                kr.alamat = item.alamat;
                                kr.no_telp = item.no_telp;
                                kr.email = item.email;
                                kr.password = item.password;
                                kr.join_date = item.join_date.ToString("MM-dd-yyyy");
                                kr.id_jabatan = item.id_jabatan;
                                kr.nama_jabatan = listJabatan[0].nama_jabatan;
                                kr.status = item.status;
                                kr.created_by = item.created_by;
                                kr.created_date = item.created_date;
                                kr.changed_by = item.changed_by;
                                kr.changed_date = item.changed_date;
                                kr.id_group = item.id_group;
                                listKrArray.Add(kr);

                            }
                            
                            listval.Add("Success");
                            listval.Add(listKrArray);
                            return listval;
                        }
                        else
                        {
                            listval.Add("Not Success");
                            listval.Add($"Data dengan ID {id} tidak ditemukan");
                            return listval;
                        }

                    }
                    else
                    {
                        listval.Add("Not Success");
                        listval.Add(dataResult.StatusCode + " " + dataResult.ReasonPhrase);
                        return listval;
                    }
                }
            }
            catch (Exception e)
            {
                listval.Add("Not Success");
                listval.Add(e.ToString());
                return listval;
            }

        }
        
        public string ChangePassword(string Username, string current_password, string new_password, string confirm_new_password)
        {
            dynamic metadata = "";
            var result = "";
            //var Res = "";
            if (new_password != confirm_new_password) {
                result = "New Password tidak sama dengan Confirm New Password";
                return result;
            }
        
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    var body = new karyawan_change_password
                    {
                        id_karyawan = Username,
                        current_password = current_password,
                        new_password = new_password,
                        changed_by = user,
                        changed_date = DateTime.Now
                    };
                    string jsonString = JsonConvert.SerializeObject(body);
                    System.Diagnostics.Debug.WriteLine(jsonString);
                    var getUser = client.PostAsync("api/Karyawan/ChangePassword", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        //return RedirectToAction("Index");
                        metadata = new
                        {
                            code = "200",
                            message = "OK"
                        };
                        System.Web.HttpContext.Current.Session["password"] = new_password;
                        result = "Success";
                        return result;
                    }
                    else {
                        result = "Current Password Not Valid";
                    }
                    
                }
                
                metadata = new
                {
                    code = "400",
                    message = "Failed to Update Data Karyawan."
                };
               // result = result;
                return result;
            }
            catch
            {
                metadata = new
                {
                    code = "400",
                    message = "Failed to Update Password."
                };
                return result;
            }
        }

        public List<dynamic> getDataMasterKaryawanByIDFromForgotPassword(string ac)
        {
            string decodeAuthInfo = Encoding.UTF8.GetString(Convert.FromBase64String(ac));
            string[] authInfoArray = decodeAuthInfo.Split(':');
            string username = authInfoArray[0];
            string password = authInfoArray[1];
            List<dynamic> listval = new List<dynamic>();
            List<MasterKaryawan> listKaryawan = new List<MasterKaryawan>();
            List<MasterKaryawanArray> listKrArray = new List<MasterKaryawanArray>();
            try
            {

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                    //Set Basic Auth
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{username}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    var getData = client.GetAsync($"api/Karyawan/Get/{username}");
                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        var Res = dataResult.Content.ReadAsStringAsync().Result;
                        listKaryawan = JsonConvert.DeserializeObject<List<MasterKaryawan>>(Res);
                        if (listKaryawan.Count > 0)
                        {
                            // Isi nama_jabatan dengan memanggil api jabatan

                            List<dynamic> listvalJabatan = MasterJabatanRepositoryF.Instance.getMasterJabatanbyID(listKaryawan[0].id_jabatan);
                            if (listvalJabatan[0].ToString() == "Success")
                            {
                                List<MasterJabatan> listJabatan = listvalJabatan[1];

                                MasterKaryawanArray kr = new MasterKaryawanArray();
                                var item = listKaryawan[0];
                                kr.id_karyawan = item.id_karyawan;
                                kr.nama = item.nama;
                                kr.alamat = item.alamat;
                                kr.no_telp = item.no_telp;
                                kr.email = item.email;
                                kr.password = item.password;
                                kr.join_date = item.join_date.ToString("MM-dd-yyyy");
                                kr.id_jabatan = item.id_jabatan;
                                kr.nama_jabatan = listJabatan[0].nama_jabatan;
                                kr.status = item.status;
                                kr.created_by = item.created_by;
                                kr.created_date = item.created_date;
                                kr.changed_by = item.changed_by;
                                kr.changed_date = item.changed_date;
                                listKrArray.Add(kr);

                            }

                            listval.Add("Success");
                            listval.Add(listKrArray);
                            return listval;
                        }
                        else
                        {
                            listval.Add("Not Success");
                            listval.Add($"Data dengan ID tidak ditemukan");
                            return listval;
                        }

                    }
                    else
                    {
                        listval.Add("Not Success");
                        listval.Add(dataResult.StatusCode + " " + dataResult.ReasonPhrase);
                        return listval;
                    }
                }
            }
            catch (Exception e)
            {
                listval.Add("Not Success");
                listval.Add(e.ToString());
                return listval;
            }

        }

        public String ValidateEmail(string email, string id_karyawan)
        {
            String validate = "";
            List<dynamic> listval = new List<dynamic>();
            List<MasterKaryawan> listKaryawan = new List<MasterKaryawan>();
            List<MasterKaryawanArray> listKrArray = new List<MasterKaryawanArray>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    string _url = "api/Karyawan/Get";

                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    var getData = client.GetAsync(_url);
                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        var Res = dataResult.Content.ReadAsStringAsync().Result;
                        listKaryawan = JsonConvert.DeserializeObject<List<MasterKaryawan>>(Res);

                        foreach (var item in listKaryawan)
                        {
                            MasterKaryawanArray kr = new MasterKaryawanArray();
                            kr.id_karyawan = item.id_karyawan;
                            kr.nama = item.nama;
                            kr.alamat = item.alamat;
                            kr.no_telp = item.no_telp;
                            kr.email = item.email;
                            kr.password = item.password;
                            kr.join_date = item.join_date.ToString("MM-dd-yyyy");
                            kr.id_jabatan = item.id_jabatan;
                            kr.nama_jabatan = item.nama_jabatan;
                            kr.status = item.status;
                            kr.created_by = item.created_by;
                            kr.created_date = item.created_date;
                            kr.changed_by = item.changed_by;
                            kr.changed_date = item.changed_date;
                            kr.id_group = item.id_group;
                            listKrArray.Add(kr);
                        }
                        listval.Add("Success");
                        listval.Add(listKrArray);

                        listKrArray = listKrArray.Where(e => e.email == email && e.id_karyawan != id_karyawan).ToList();

                        if (listKrArray.Count > 0)
                        {
                            validate = "Email Karyawan Sudah Terdaftar pada ID " + listKrArray[0].id_karyawan;
                        }
                        else
                        {
                            validate = "Success";
                        }
                        return validate;

                    }
                    listval.Add("Not Success");
                    listval.Add(dataResult.StatusCode + " " + dataResult.ReasonPhrase);
                    return validate = "Not Success";
                }
            }
            catch (Exception e)
            {
                listval.Add("Not Success");
                listval.Add(e.ToString());
                return validate = "Not Success";
            }

        }
    }
}