﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExploreApilkasiCuti.Models
{
    public class MasterLogApproval
    {
        public string id_log_approval { get; set; }
        public string id_pengajuan { get; set; }
        public string id_approval { get; set; }
        public string approved_by { get; set; }
        public string approve_status { get; set; }
        public Nullable<DateTime> approve_date { get; set; }
        public string created_by { get; set; }
        public Nullable<DateTime> created_date { get; set; }
    }

    public class MasterLogApprovalJoin
    {
        public string id_log_approval { get; set; }
        public string id_pengajuan { get; set; }
        public string id_approval { get; set; }
        public string nama_approval { get; set; }
        public string approved_by { get; set; }
        public string nama_karyawan_approval { get; set; }
        public string approve_status { get; set; }
        public Nullable<DateTime> approve_date { get; set; }
        public string created_by { get; set; }
        public Nullable<DateTime> created_date { get; set; }
    }
}