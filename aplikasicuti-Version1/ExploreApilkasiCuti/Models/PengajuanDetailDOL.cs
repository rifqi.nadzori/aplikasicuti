﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ExploreApilkasiCuti.Models
{
    public class PengajuanDetailDOL
    {
        public string id_pengajuan_detail { get; set; }
        public string id_pengajuan { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
        public string durasi { get; set; }
    }
}