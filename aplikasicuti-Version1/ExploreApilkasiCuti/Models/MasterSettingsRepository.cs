﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace ExploreApilkasiCuti.Models
{
    public class MasterSettingsRepository
    {
        private static MasterSettingsRepository instance = null;
        public static MasterSettingsRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MasterSettingsRepository();
                }

                return instance;
            }
        }

        public List<dynamic> getMasterSettingbyID(string id)
        {
            List<dynamic> listval = new List<dynamic>();
            List<MasterSettings> listSettings = new List<MasterSettings>();
            List<MasterSettingsArray> listStArray = new List<MasterSettingsArray>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    string _url = "api/Settings/Get/{id}";
                    //if (_idJabatan != "" && _idJabatan != null) _url += $"idjabatan={_idJabatan}&";
                    

                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    var getData = client.GetAsync(_url);
                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        var Res = dataResult.Content.ReadAsStringAsync().Result;
                        listSettings = JsonConvert.DeserializeObject<List<MasterSettings>>(Res);

                        foreach (var item in listSettings)
                        {
                            MasterSettingsArray kr = new MasterSettingsArray();
                            kr.id_setting = item.id_setting;
                            kr.deskripsi = item.deskripsi;
                            kr.value_settings = item.value_settings;
                            kr.created_by = item.created_by;
                            kr.created_date = item.created_date;
                            kr.changed_by = item.changed_by;
                            kr.changed_date = item.changed_date;
                            listStArray.Add(kr);
                        }
                        listval.Add("Success");
                        listval.Add(listStArray);
                        return listval;

                    }
                    listval.Add("Not Success");
                    listval.Add(dataResult.StatusCode + " " + dataResult.ReasonPhrase);
                    return listval;
                }
            }
            catch (Exception e)
            {
                listval.Add("Not Success");
                listval.Add(e.ToString());
                return listval;
            }
        }
    }
}