﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace ExploreApilkasiCuti.Models
{
    public class MasterJenisPengajuanRepositoryA
    {
        private static MasterJenisPengajuanRepositoryA instance = null;
        public static MasterJenisPengajuanRepositoryA Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MasterJenisPengajuanRepositoryA();
                }

                return instance;
            }
        }

        public List<dynamic> getMasterJenisPengajuanbyID(string id)
        {
            List<dynamic> listval = new List<dynamic>();
            List<MasterJenisPengajuan> listJenisPengajuan = new List<MasterJenisPengajuan>();
            try
            {

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    var getData = client.GetAsync($"api/JenisPengajuan/Get/{id}");
                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        var Res = dataResult.Content.ReadAsStringAsync().Result;
                        listJenisPengajuan = JsonConvert.DeserializeObject<List<MasterJenisPengajuan>>(Res);
                        if (listJenisPengajuan.Count > 0)
                        {
                            listval.Add("Success");
                            listval.Add(listJenisPengajuan);
                            return listval;
                        }
                        else
                        {
                            listval.Add("Not Success");
                            listval.Add($"Data dengan ID {id} tidak ditemukan");
                            return listval;
                        }

                    }
                    else
                    {
                        listval.Add("Not Success");
                        listval.Add(dataResult.StatusCode + " " + dataResult.ReasonPhrase);
                        return listval;
                    }
                }
            }
            catch (Exception e)
            {
                listval.Add("Not Success");
                listval.Add(e.ToString());
                return listval;
            }
        }
    }
}