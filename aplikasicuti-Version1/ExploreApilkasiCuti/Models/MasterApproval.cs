﻿using System;

namespace ExploreApilkasiCuti.Models
{
    public class MasterApproval
    {
        public string id_approval { get; set; }
        public string nama_approval { get; set; }
        public bool mandatory { get; set; }
        public bool finish { get; set; }
        public string status { get; set; }
        public string created_by { get; set; }
        public Nullable<DateTime> created_date { get; set; }
        public string changed_by { get; set; }
        public Nullable<DateTime> changed_date { get; set; }
    }
}