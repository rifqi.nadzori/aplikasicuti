﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ExploreApilkasiCuti.Models
{
    public partial class LogApprovalMdl
    {
        public string id_log_approval { get; set; }
        public string id_pengajuan { get; set; }
        public string id_approval { get; set; }
        public string approved_by { get; set; }
        public string approve_status { get; set; }
        public Nullable<System.DateTime> approve_date { get; set; }
        public string created_by { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public string nama_approval { get; set; }
    }
}