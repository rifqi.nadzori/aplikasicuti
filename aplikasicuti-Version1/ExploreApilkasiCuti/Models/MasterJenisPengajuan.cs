﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExploreApilkasiCuti.Models
{
    public class MasterJenisPengajuan
    {
        public string id_jenis_pengajuan { get; set; }
        public string nama_pengajuan { get; set; }
        public string durasi { get; set; }
        public string created_by { get; set; }
        public DateTime created_date { get; set; }
        public string changed_by { get; set; }
        public string changed_date { get; set; }
    }
}