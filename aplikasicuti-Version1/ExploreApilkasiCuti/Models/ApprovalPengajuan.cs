﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExploreApilkasiCuti.Models
{
    public class ApprovalPengajuan
    {
        public string id_pengajuan { get; set; }
        public string id_karyawan { get; set; }
        public string nama { get; set; }
        public string id_jenis_pengajuan { get; set; }
        public string nama_pengajuan { get; set; }
        public Nullable<DateTime> tgl_pengajuan { get; set; }
        public decimal durasi { get; set; }
        public Nullable<bool> denda_cuti { get; set; }
        public string alamat_cuti { get; set; }
        public string notelp_cuti { get; set; }
        public string keterangan_pengajuan { get; set; }
        public string approve_status { get; set; }
        public string keterangan_approve { get; set; }
        public string created_by { get; set; }
        public Nullable<DateTime> created_date { get; set; }
        public string changed_by { get; set; }
        public Nullable<DateTime> changed_date { get; set; }
        public string nama_file { get; set; }
        public string link_file { get; set; }
        public List<PengajuanCutiDetailArray> details { get; set; }
    }

    public class ApprovalPengajuanArray
    {
        public int no_urut { get; set; }
        public string id_pengajuan { get; set; }
        public string id_karyawan { get; set; }
        public string nama { get; set; }
        public string id_jenis_pengajuan { get; set; }
        public string nama_pengajuan { get; set; }
        public string tgl_pengajuan { get; set; }
        public decimal durasi { get; set; }
        public Nullable<bool> denda_cuti { get; set; }
        public string alamat_cuti { get; set; }
        public string notelp_cuti { get; set; }
        public string keterangan_pengajuan { get; set; }
        public string approve_status { get; set; }
        public string keterangan_approve { get; set; }
        public string created_by { get; set; }
        public string created_date { get; set; }
        public string changed_by { get; set; }
        public string changed_date { get; set; }
        public string action { get; set; }
        public string nama_file { get; set; }
        public string link_file { get; set; }
        public List<PengajuanCutiDetailArray> details { get; set; }
    }

    public class NewApprovalPengajuan
    {
        public string id_karyawan_approval { get; set; }
        public NewPengajuanCuti pengajuan { get; set; }
        public string new_id_approval { get; set; }
        public string new_approved_by { get; set; }
    }
}