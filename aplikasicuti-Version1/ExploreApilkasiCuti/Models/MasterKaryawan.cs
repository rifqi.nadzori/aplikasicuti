﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExploreApilkasiCuti.Models
{
    public class MasterKaryawan
    {
        public string id_karyawan { get; set; }
        public string nama { get; set; }
        public string alamat { get; set; }
        public string no_telp { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        //[DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime join_date { get; set; }
        public string id_jabatan { get; set; }
        public string nama_jabatan { get; set; }
        public string status { get; set; }
        public string created_by { get; set; }
        public Nullable<DateTime> created_date { get; set; }
        public string changed_by { get; set; }
        public Nullable<DateTime> changed_date { get; set; }
        public string id_group { get; set; }
    }
    public class MasterKaryawanArray
    {
        public string id_karyawan { get; set; }
        public string nama { get; set; }
        public string alamat { get; set; }
        public string no_telp { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        //[DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public string join_date { get; set; }
        public string id_jabatan { get; set; }
        public string nama_jabatan { get; set; }
        public string status { get; set; }
        public string created_by { get; set; }
        public Nullable<DateTime> created_date { get; set; }
        public string changed_by { get; set; }
        public Nullable<DateTime> changed_date { get; set; }
        public string id_group { get; set; }
    }
    public class karyawan_change_password {
        public string id_karyawan { get; set; }
        public string current_password { get; set; }
        public string new_password { get; set; }
        public string changed_by { get; set; }
        public Nullable<DateTime> changed_date { get; set; }
    }
}