﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExploreApilkasiCuti.Models
{
    public class MasterKaryawanApproval
    {
        public string id_karyawan_approval { get; set; }
        public string id_approval { get; set; }
        public string id_karyawan { get; set; }
        public string created_by { get; set; }
        public Nullable<DateTime> created_date { get; set; }

    }
    public class MasterKaryawanApprovalArray
    {
        public string id_karyawan_approval { get; set; }
        public string id_approval { get; set; }
        public string id_karyawan { get; set; }
        public string created_by { get; set; }
        public Nullable<DateTime> created_date { get; set; }

    }

    public class MasterKaryawanApprovalJoin
    {
        public string id_karyawan_approval { get; set; }
        public string id_approval { get; set; }
        public string id_karyawan { get; set; }
        public string created_by { get; set; }
        public Nullable<DateTime> created_date { get; set; }
        public string nama { get; set; }
    }
}
