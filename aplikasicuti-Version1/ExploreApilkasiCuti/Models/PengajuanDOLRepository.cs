﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace ExploreApilkasiCuti.Models
{
    public class PengajuanDOLRepository
    {
        private static PengajuanDOLRepository instance = null;
        public static PengajuanDOLRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PengajuanDOLRepository();
                }

                return instance;
            }
        }

        public List<dynamic> getDataPengajuanDOL()
        {

            List<dynamic> listval = new List<dynamic>();
            List<PengajuanDOL> listPengajuanDOL = new List<PengajuanDOL>();
            List<PengajuanDOLArray> listKrArray = new List<PengajuanDOLArray>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    var getData = client.GetAsync($"api/Pengajuan/Get");
                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        var Res = dataResult.Content.ReadAsStringAsync().Result;
                        listPengajuanDOL = JsonConvert.DeserializeObject<List<PengajuanDOL>>(Res);

                        foreach (var item in listPengajuanDOL)
                        {
                            PengajuanDOLArray pjn = new PengajuanDOLArray();
                            pjn.id_pengajuan = item.id_pengajuan;
                            pjn.id_karyawan = item.id_karyawan;
                            pjn.tgl_pengajuan = item.tgl_pengajuan.ToString("MM-dd-yyyy");
                            pjn.id_karyawan = item.id_karyawan;
                            pjn.id_jenis_pengajuan = item.id_jenis_pengajuan;
                            pjn.durasi = item.durasi;
                            pjn.keterangan_pengajuan = item.keterangan_pengajuan;
                            pjn.approve_status = item.approve_status;
                            pjn.keterangan_approve = item.keterangan_approve;
                            pjn.created_by = item.created_by;
                            pjn.created_date = item.created_date;
                            pjn.changed_by = item.changed_by;
                            pjn.changed_date = item.changed_date;
                            listKrArray.Add(pjn);
                        }
                        listval.Add("Success");
                        listval.Add(listKrArray);
                        return listval;

                    }
                    listval.Add("Not Success");
                    listval.Add(dataResult.StatusCode + " " + dataResult.ReasonPhrase);
                    return listval;
                }
            }
            catch (Exception e)
            {
                listval.Add("Not Success");
                listval.Add(e.ToString());
                return listval;
            }

        }
        public List<dynamic> getPengajuanDOLbyID(string id)
        {
            List<dynamic> listval = new List<dynamic>();
            List<PengajuanDOL> listPjn = new List<PengajuanDOL>();
            try
            {

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    var getData = client.GetAsync($"api/Pengajuan/Get/{id}");
                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        var Res = dataResult.Content.ReadAsStringAsync().Result;
                        listPjn = JsonConvert.DeserializeObject<List<PengajuanDOL>>(Res);
                        if (listPjn.Count > 0)
                        {
                            listval.Add("Success");
                            listval.Add(listPjn);
                            return listval;
                        }
                        else
                        {
                            listval.Add("Not Success");
                            listval.Add($"Data dengan ID {id} tidak ditemukan");
                            return listval;
                        }

                    }
                    else
                    {
                        listval.Add("Not Success");
                        listval.Add(dataResult.StatusCode + " " + dataResult.ReasonPhrase);
                        return listval;
                    }
                }
            }
            catch (Exception e)
            {
                listval.Add("Not Success");
                listval.Add(e.ToString());
                return listval;
            }
        }

        public List<dynamic> getDataPengajuanDOLByID(string id)
        {
            List<dynamic> listval = new List<dynamic>();
            List<PengajuanDOL> listPjn = new List<PengajuanDOL>();
            List<PengajuanDOLArray> listKrArray = new List<PengajuanDOLArray>();
            try
            {

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");

                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    var getData = client.GetAsync($"api/Pengajuan/Get/{id}");
                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        var Res = dataResult.Content.ReadAsStringAsync().Result;
                        listPjn = JsonConvert.DeserializeObject<List<PengajuanDOL>>(Res);
                        if (listPjn.Count > 0)
                        {
                            // Isi nama_jabatan dengan memanggil api jabatan

                            List<dynamic> listJenisPengajuan = MasterJenisPengajuanRepositoryA.Instance.getMasterJenisPengajuanbyID(listPjn[0].id_jenis_pengajuan);
                            if (listJenisPengajuan[0].ToString() == "Success")
                            {
                                List<PengajuanDOL> listValPjn = listJenisPengajuan[1];

                                PengajuanDOLArray pjn = new PengajuanDOLArray();
                                var item = listPjn[0];
                                pjn.id_karyawan = item.id_karyawan;
                                pjn.id_pengajuan = item.id_pengajuan;
                                pjn.tgl_pengajuan = item.tgl_pengajuan.ToString("MM-dd-yyyy");
                                pjn.id_karyawan = item.id_karyawan;
                                pjn.id_jenis_pengajuan = item.id_jenis_pengajuan;
                                pjn.durasi = item.durasi;
                                pjn.keterangan_pengajuan = item.keterangan_pengajuan;
                                pjn.approve_status = item.approve_status;
                                pjn.keterangan_approve = item.keterangan_approve;
                                pjn.created_by = item.created_by;
                                pjn.created_date = item.created_date;
                                pjn.changed_by = item.changed_by;
                                pjn.changed_date = item.changed_date;
                                listKrArray.Add(pjn);

                            }

                            listval.Add("Success");
                            listval.Add(listKrArray);
                            return listval;
                        }
                        else
                        {
                            listval.Add("Not Success");
                            listval.Add($"Data dengan ID {id} tidak ditemukan");
                            return listval;
                        }

                    }
                    else
                    {
                        listval.Add("Not Success");
                        listval.Add(dataResult.StatusCode + " " + dataResult.ReasonPhrase);
                        return listval;
                    }
                }
            }
            catch (Exception e)
            {
                listval.Add("Not Success");
                listval.Add(e.ToString());
                return listval;
            }
        }
    }
}