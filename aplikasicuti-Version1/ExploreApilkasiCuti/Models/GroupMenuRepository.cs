﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace ExploreApilkasiCuti.Models
{
    public class GroupMenuRepository
    {
        private static GroupMenuRepository instance = null;
        public static GroupMenuRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GroupMenuRepository();
                }

                return instance;
            }
        }
        public String getTreeView(string GroupID)
        {
            string _treeview = "";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                //Set Basic Auth
                var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                var password = System.Web.HttpContext.Current.Session["password"].ToString();
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);


                var getUser = client.PostAsync("api/GroupMenu/GetMenusTreeview?idgroup=" + GroupID, new StringContent("", Encoding.UTF8, "application/json"));

                getUser.Wait();

                var userResult = getUser.Result;
                if (userResult.IsSuccessStatusCode)
                {
                    var Res = userResult.Content.ReadAsStringAsync().Result;
                    _treeview = JsonConvert.DeserializeObject(Res).ToString();
                }
                return _treeview;
            }
        }
        

        public List<dynamic> saveGroupMenu(string[] MenuID, string GroupID)
        {
            List<dynamic> _listval = new List<dynamic>();
            List<dynamic> _listreturn = new List<dynamic>();
            try
            {
                _listval = deleteGroupMenuByGroup(GroupID);
                if (_listval[0] == "Success") _listval = addGroupMenu(MenuID, GroupID);
                if (_listval[0] == "Success") 
                {
                    _listreturn.Add("Success");
                }
                else
                {
                    _listreturn.Add("Not Success");
                    _listreturn.Add(_listval[1].ToString());
                }

                return _listreturn;
            }
            catch(Exception e)
            {
                _listreturn.Add("Not Success");
                _listreturn.Add(e.ToString());
                return _listreturn;
            }
        }
        public List<dynamic> deleteGroupMenuByGroup(string GroupID)
        {
            List<dynamic> _listval = new List<dynamic>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    var body = new GroupMenu
                    {
                        id_group = GroupID
                    };
                    string jsonString = JsonConvert.SerializeObject(body);
                    var getUser = client.PostAsync("api/GroupMenu/DeleteByGroup", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        _listval.Add("Success");
                    }
                    else
                    {
                        _listval.Add("Not Success");
                        _listval.Add("Gagal mengupdate hak akses. " + userResult.ReasonPhrase);
                    }
                    return _listval;
                }
            }
            catch (Exception e)
            {
                _listval.Add("Not Success");
                _listval.Add(e.ToString());
                return _listval;
            }
        }
        public List<dynamic> addGroupMenu(string[] MenuID, string GroupID)
        {
            List<dynamic> _listval = new List<dynamic>();
            try
            {
                List<GroupMenu> _listGroupMenu = new List<GroupMenu>();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var password = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    foreach (var item in MenuID)
                    {
                        var _groupMenu = new GroupMenu
                        {
                            id_group_menu = GroupID + item,
                            id_group = GroupID,
                            id_menu = item,
                            created_by = user,
                            created_date = DateTime.Now,
                        };
                        _listGroupMenu.Add(_groupMenu);
                    }
                    string jsonString = JsonConvert.SerializeObject(_listGroupMenu);
                    var getUser = client.PostAsync("api/GroupMenu/CreateGroupMenus", new StringContent(jsonString, Encoding.UTF8, "application/json"));

                    getUser.Wait();

                    var userResult = getUser.Result;
                    if (userResult.IsSuccessStatusCode)
                    {
                        _listval.Add("Success");
                    }
                    else
                    {
                        _listval.Add("Not Success");
                        _listval.Add("Gagal mengupdate hak akses. " + userResult.ReasonPhrase);
                    }
                    return _listval;
                }
            }
            catch (Exception e)
            {
                _listval.Add("Not Success");
                _listval.Add(e.ToString());
                return _listval;
            }
        }

        public List<MasterMenuArray> getDatabyLink(string linkMenu)
        {
            var menu = "";
            //Master Menu

            List<MasterMenu> listMtMenu = new List<MasterMenu>();
            List<MasterMenuArray> listDataMenuLink = new List<MasterMenuArray>();

            try
            {
                using (var client = new HttpClient())
                {
                    //Set Basic Auth
                    var user = System.Web.HttpContext.Current.Session["id_karyawan"].ToString();
                    var pass = System.Web.HttpContext.Current.Session["password"].ToString();
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{pass}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);

                    client.BaseAddress = new Uri("http://172.16.19.18/CutiInta/");
                    var getData = client.GetAsync("api/Menu/Get");
                    getData.Wait();

                    var dataResult = getData.Result;
                    if (dataResult.IsSuccessStatusCode)
                    {
                        var res = dataResult.Content.ReadAsStringAsync().Result;
                        listMtMenu = JsonConvert.DeserializeObject<List<MasterMenu>>(res);
                        for (int i = 0; i < listMtMenu.Count; i++)
                        {
                            if (listMtMenu[i].created_date != null) listMtMenu[i].created_date = listMtMenu[i].created_date.Replace("T00:00:00", "");
                            if (listMtMenu[i].changed_date != null) listMtMenu[i].changed_date = listMtMenu[i].changed_date.Replace("T00:00:00", "");

                            string action = "<button type='button' class='btn btn-sm btn-icon btn-icon rounded-circle bg-gradient-primary waves-effect waves-light' style='margin-right: 5px;' onclick=edit_menu('" + listMtMenu[i].id_menu + "')><i class='fas fa-edit'></i></button>" + "  " +
                                            "<button type='button' class='btn btn-sm btn-icon btn-icon rounded-circle bg-gradient-danger waves-effect waves-light' onclick=delete_menu('" + listMtMenu[i].id_menu + "')><i class='fa fa-trash-alt'></i></button>";

                            listDataMenuLink.Add(new MasterMenuArray()
                            {
                                id_menu = listMtMenu[i].id_menu,
                                id_parent = listMtMenu[i].id_parent,
                                nama_menu = listMtMenu[i].nama_menu,
                                link = listMtMenu[i].link,
                                icon = listMtMenu[i].icon,
                                created_by = listMtMenu[i].created_by,
                                created_date = listMtMenu[i].created_date,
                                changed_by = listMtMenu[i].changed_by,
                                changed_date = listMtMenu[i].changed_date,
                                IsParent = listMtMenu[i].IsParent,
                                MenuOrder = listMtMenu[i].MenuOrder,
                                MenuLevel = listMtMenu[i].MenuLevel,
                                action = action
                            });
                        }


                    }
                }
                listDataMenuLink = listDataMenuLink.Where(e => e.link.ToLower() == linkMenu.ToLower()).ToList();
                return listDataMenuLink;
            }
            catch
            {
                return listDataMenuLink;
            }
        }

    }
}