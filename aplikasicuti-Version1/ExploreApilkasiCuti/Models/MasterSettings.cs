﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExploreApilkasiCuti.Models
{
    public class MasterSettings
    {
        
        public string id_setting { get; set; }
        public string deskripsi { get; set; }
        public string value_settings { get; set; }
        public string created_by { get; set; }
        public Nullable<DateTime> created_date { get; set; }
        public string changed_by { get; set; }
        public Nullable<DateTime> changed_date { get; set; }
    }
    public class MasterSettingsArray
    {
        public string id_setting { get; set; }
        public string deskripsi { get; set; }
        public string value_settings { get; set; }
        public string created_by { get; set; }
        public Nullable<DateTime> created_date { get; set; }
        public string changed_by { get; set; }
        public Nullable<DateTime> changed_date { get; set; }
    }

}